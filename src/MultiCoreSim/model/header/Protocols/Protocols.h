/*
 * File  :      Protocols.h
 * Author:      Mohammed Ismail
 * Email :      ismaim22@mcmaster.ca
 *
 * Created On Dec 10, 2021
 */

#ifndef _PROTOCOLS_H
#define _PROTOCOLS_H

#include "CoherenceProtocolHandler.h"

#include "LLCMESIProtocol.h"
#include "LLCMSIProtocol.h"
#include "LLCPMSIProtocol.h"
#include "LLCPMESIProtocol.h"

#include "MSIProtocol.h"
#include "MESIProtocol.h"
#include "MOESIProtocol.h"
#include "PMSIProtocol.h"
#include "PMESIProtocol.h"
#include "PMSIAsteriskProtocol.h"
#include "PMESIAsteriskProtocol.h"

#endif