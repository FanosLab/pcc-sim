/*
 * File  :      LLCMESIProtocol.h
 * Author:      Mohammed Ismail
 * Email :      ismaim22@mcmaster.ca
 *
 * Created On Nov 28, 2021
 */

#ifndef _LLCMESIProtocol_H
#define _LLCMESIProtocol_H

#include "LLCMSIProtocol.h"

namespace ns3
{
    class LLCMESIProtocol : public LLCMSIProtocol
    {
    protected:
        enum class ActionId
        {
            Stall = 0,
            GetData,
            SendData,
            SaveData,
            SetOwner,
            ClearOwner,
            IssueInv,
            WriteBack,
            Fault,
            SendExeclusiveData,
        };

        virtual std::vector<ControllerAction> &handleAction(std::vector<int> &actions, Message &msg,
                                                            const GenericCache::CacheLineInfo &cache_line_info, int next_state) override;

    public:
        LLCMESIProtocol(GenericCache *privCache, const std::string &fsm_path, int coreId);
        ~LLCMESIProtocol();

        void createDefaultCacheLine(uint64_t address, GenericCacheFrmt *cache_line) override;
    };
}

#endif
