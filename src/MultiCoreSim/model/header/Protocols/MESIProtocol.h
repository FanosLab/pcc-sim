/*
 * File  :      MESIProtocol.h
 * Author:      Mohammed Ismail
 * Email :      ismaim22@mcmaster.ca
 *
 * Created On Nov 28, 2021
 */

#ifndef _MESIProtocol_H
#define _MESIProtocol_H

#include "MSIProtocol.h"

namespace ns3
{
    class MESIProtocol : public MSIProtocol
    {
    protected:
        enum class EventId
        {
            Load = 0,
            Store,
            Replacement,

            Own_GetS,
            Own_GetM,
            Own_PutM,

            Other_GetS,
            Other_GetM,
            Other_PutM,

            OwnData,
            OwnData_Execlusive,
        };

        virtual void readEvent(Message &msg, MSIProtocol::EventId *out_id) override;

    public:
        MESIProtocol(GenericCache *privCache, const std::string &fsm_path, int coreId, int sharedMemId, int reqWbRatio, bool cache2Cache);
        ~MESIProtocol();

        virtual FRFCFS_State getRequestState(const Message &, FRFCFS_State) override;
    };
}

#endif
