/*
 * File  :      MSIProtocol.h
 * Author:      Mohammed Ismail
 * Email :      ismaim22@mcmaster.ca
 *
 * Created On June 23, 2021
 */

#ifndef _MSIProtocol_H
#define _MSIProtocol_H

#include "CoherenceProtocolHandler.h"

namespace ns3
{
    class MSIProtocol : public CoherenceProtocolHandler
    {
    protected:
        enum class EventId
        {
            Load = 0,
            Store,
            Replacement,

            Own_GetS,
            Own_GetM,
            Own_PutM,

            Other_GetS,
            Other_GetM,
            Other_PutM,

            OwnData,
        };

        enum class ActionId
        {
            Stall = 0,
            Hit,
            GetS,
            GetM,
            PutM,
            Data2Req,
            Data2Both,
            SaveReq,
            Fault
        };

        std::vector<ControllerAction> controller_actions; //used only for returning data

        virtual void readEvent(Message &msg, EventId *out_id);
        virtual void handleNonExistingLine(GenericCache::CacheLineInfo &cache_line_info, Message **msg);

        virtual std::vector<ControllerAction> &handleAction(std::vector<int> &actions, Message &msg,
                                                            const GenericCache::CacheLineInfo &cache_line_info, int next_state);

    public:
        MSIProtocol(GenericCache *privCache, const std::string &fsm_path, int coreId, int sharedMemId, int reqWbRatio, bool cache2Cache);
        ~MSIProtocol();

        virtual const std::vector<ControllerAction> &processRequest(Message &request_msg) override;
        virtual FRFCFS_State getRequestState(const Message &, FRFCFS_State) override;
    };
}

#endif
