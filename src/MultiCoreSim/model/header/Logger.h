/*
 * File  :      Logger.h
 * Author:      Mohammed Ismail
 * Email :      ismaim22@mcmaster.ca
 *
 * Created On Sep 17, 2021
 */

#ifndef LOGGER_H
#define LOGGER_H

#include "MemTemplate.h"

#include <stdint.h>
#include <string>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <map>

#define NUM_OF_ELEMENTS_PER_ENTRY   10

namespace ns3
{
    class Logger
    {
    public:
        enum class EntryId
        {
            CPU_ID = 0,
            REQ_ID,
            REQ_ADDRESS,
            TRACE_CYCLE,
            CPU_CHECKPOINT,
            REPLACE_ACTION_CHECKPOINT,
            CACHE_CHECKPOINT,
            REQ_BUS_CHECKPOINT,
            RESP_BUS_CHECKPOINT,
            CPU_RX_CHECKPOINT
        };

    protected:
        std::map<uint64_t, uint64_t*> log_entries; //msg_id is the key, and the value is an uint64_t array

        std::map<uint64_t, uint64_t> worst_case_req_bus_latency; //core_id is the key, and the value is the latency
        std::map<uint64_t, uint64_t> worst_case_resp_bus_latency; //core_id is the key, and the value is the latency
        std::map<uint64_t, uint64_t> worst_case_replace_latency; //core_id is the key, and the value is the latency
        std::map<uint64_t, uint64_t> worst_case_latency_minus_replace; //core_id is the key, and the value is the latency
        std::map<uint64_t, uint64_t> worst_bus_latency; //core_id is the key, and the value is the latency
        std::map<uint64_t, uint64_t> worst_case_latency; //core_id is the key, and the value is the latency
        std::map<uint64_t, uint64_t> max_effective_latency; //core_id is the key, and the value is the max latency contribution
        std::map<uint64_t, uint64_t> average_latency; //core_id is the key, and the value is the latency
        std::map<uint64_t, uint64_t> num_request; //core_id is the key, number of requests
        std::map<uint64_t, uint64_t> last_cycle; //core_id is the key, the last cycle of the core

        std::map<uint64_t, std::ofstream> report_files; //core_id is the key, and the value is the report file handler
        std::ofstream summary_file;                     //To report the worst-case values of all the cores
        
        std::map<uint64_t, uint64_t> core_clk_count; //core_id is the key, and the value is the report file handler

        std::map<uint64_t, uint64_t> last_checkpoint;   //This value is used to caculate the contribution of each request to the total program time
        
        uint64_t replacement_correction; //This value is to correct the replacement lactency by offset this value from the request latency and add it to the replacement

        std::string report_file_path;
        static Logger *_logger;

        Logger();
        void prepareReportFile(uint64_t core_id);
        void initializeStats(uint64_t core_id);
        void calculateLatencies(uint64_t msg_id);

        uint64_t writeLatency(std::ofstream&, uint64_t, uint64_t); //int64_t is used instead of uint64_t to keep the sign after subtraction
        uint64_t writeLatency(std::ofstream&, uint64_t *, EntryId);

    public:
        void addRequest(uint64_t cpu_id, CpuFIFO::ReqMsg&);
        void updateRequest(uint64_t msg_id, EntryId entryId);
        void registerReportPath(std::string file_path);
        void traceEnd(uint64_t core_id);
        void setClkCount(uint64_t core_id, uint64_t clk);
        void setReplacementCorrection(uint64_t cycles);
        void registerReplacementMessage(uint64_t cpu_id);

        static Logger *getLogger()
        {
            if (Logger::_logger == NULL)
                Logger::_logger = new Logger();
            return Logger::_logger;
        }
    };
}

#endif