/*
 * File  :      CacheController.h
 * Author:      Mohammed Ismail
 * Email :      ismaim22@mcmaster.ca
 *
 * Created On June 23, 2021
 */

#ifndef _CacheController_H
#define _CacheController_H

#include "ns3/ptr.h"
#include "ns3/object.h"
#include "ns3/core-module.h"
#include "CommunicationInterface.h"
#include "SNOOPPrivCohProtocol.h"
#include "GenericCache.h"
#include "CacheXml.h"

#include "ns3/Protocols.h"
#include "FRFCFS_Buffer.h"
#include "Logger.h"

#include <string>
#include <queue>
#include <vector>
#include <map>

namespace ns3
{

    class CacheController : public ns3::Object
    {
    private:
        typedef void (CacheController::* ActionFunction)(void *);

        int m_core_id;
        int m_shared_memory_id;
        int m_cache_line_size;

        double m_dt;
        double m_clk_skew;
        uint64_t m_cache_cycle;
        ActionFunction *m_actions_array;

        CommunicationInterface* m_lower_interface;     // A pointer to the lower Interface FIFO
        CommunicationInterface* m_upper_interface;   // A pointer to the upper Interface FIFO

        GenericCache *m_cache;                // A pointer to Private cache
        CoherenceProtocolHandler *m_protocol; // A pointer to Cache Coherence Protocol

        //This queue is used mainly to serialize messages that come from different sources
        FRFCFS_Buffer<Message, CoherenceProtocolHandler> *m_processing_queue;

        //key is the msg.addr & mask(nbits of CacheLineSize) and the value is queue of Messages
        //to ensure order of requests of the same cache line
        std::map<uint64_t, std::queue<Message>> m_pending_cpu_requests;

        //key is the msg.addr & mask(nbits of CacheLineSize) and the value is request Message
        std::map<uint64_t, Message> m_saved_requests_for_wb; 

        void cycleProcess();
        void processLogic();
        void addRequests2ProcessingQueue(FRFCFS_Buffer<Message, CoherenceProtocolHandler> &);

        uint64_t getAddressKey(uint64_t addr);

        void removePendingAndRespond(void *);
        void hitAction(void *);
        void addtoPendingRequests(void *);
        void sendBusRequest(void *);
        void performWriteBack(void *);
        void updateCacheLine(void *);
        void saveReqForWriteBack(void *);
        void noAction(void *) {}; //empty function

    public:
        static TypeId GetTypeId(void); // Override TypeId.

        CacheController(CacheXml &cacheXml, string &fsm_path,
                        CommunicationInterface* upper_interface, CommunicationInterface* lower_interface,
                        bool cach2Cache, int sharedMemId, CohProtType pType);
        ~CacheController();

        void init();
        void initializeCacheData(std::vector<std::string> &tracePaths);
        static void step(Ptr<CacheController> cache_controller);
    };
}

#endif /* _CacheController_H */
