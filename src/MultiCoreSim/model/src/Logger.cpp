/*
 * File  :      Logger.cpp
 * Author:      Mohammed Ismail
 * Email :      ismaim22@mcmaster.ca
 *
 * Created On Sep 17, 2021
 */

#include "../header/Logger.h"

using namespace std;
namespace ns3
{

    Logger *Logger::_logger = NULL;

    Logger::Logger()
    {
    }

    void Logger::addRequest(uint64_t cpu_id, CpuFIFO::ReqMsg &entry)
    {
        log_entries[entry.msgId] = new uint64_t[NUM_OF_ELEMENTS_PER_ENTRY];
        memset(log_entries[entry.msgId], 0, NUM_OF_ELEMENTS_PER_ENTRY * sizeof(uint64_t));

        log_entries[entry.msgId][(int)EntryId::CPU_ID] = cpu_id;
        log_entries[entry.msgId][(int)EntryId::REQ_ID] = entry.msgId;
        log_entries[entry.msgId][(int)EntryId::REQ_ADDRESS] = entry.addr;
        log_entries[entry.msgId][(int)EntryId::TRACE_CYCLE] = entry.cycle;
        log_entries[entry.msgId][(int)EntryId::CPU_CHECKPOINT] = entry.fifoInserionCycle;

        initializeStats(cpu_id);
    }

    void Logger::updateRequest(uint64_t msg_id, EntryId entryId)
    {
        if (msg_id == 0 || log_entries.find(msg_id) == log_entries.end())
            return; //ignore updates with no ID (it happens in the case of replacement action) and updates for unpresent messages (can be generated from Shared memory)

        uint64_t core_id = log_entries[msg_id][(int)EntryId::CPU_ID];

        if (log_entries[msg_id][(int)entryId] == 0) //updated once
            log_entries[msg_id][(int)entryId] = core_clk_count[core_id];

        if (entryId == EntryId::CPU_RX_CHECKPOINT)
        {
            last_cycle[core_id] = core_clk_count[core_id];
            calculateLatencies(msg_id);
        }
        else if (entryId == EntryId::REPLACE_ACTION_CHECKPOINT)
        {
            //clear cache checkpoint since the request will be reissued again
            log_entries[msg_id][(int)EntryId::CACHE_CHECKPOINT] = 0;
        }
    }

    void Logger::registerReplacementMessage(uint64_t cpu_id)
    {
        this->last_checkpoint[cpu_id] = core_clk_count[cpu_id] + 1; //update last checkpoint with after data response of a replacement action
    }

    void Logger::calculateLatencies(uint64_t msg_id)
    {
        uint64_t returned_latency = 0;
        uint64_t request_latency, respond_latency, replacement_latency;
        uint64_t core_id = log_entries[msg_id][(int)EntryId::CPU_ID];

        this->prepareReportFile(core_id);

        report_files[core_id] << log_entries[msg_id][(int)EntryId::REQ_ID] << ",";
        report_files[core_id] << std::hex << log_entries[msg_id][(int)EntryId::REQ_ADDRESS] << "," << std::dec;
        report_files[core_id] << log_entries[msg_id][(int)EntryId::TRACE_CYCLE] << ",";

        writeLatency(report_files[core_id], log_entries[msg_id][(int)EntryId::CPU_CHECKPOINT],
                     log_entries[msg_id][(int)EntryId::TRACE_CYCLE]); //CPU Latency
        if (log_entries[msg_id][(int)EntryId::REPLACE_ACTION_CHECKPOINT] == 0)
        {
            writeLatency(report_files[core_id], log_entries[msg_id][(int)EntryId::CACHE_CHECKPOINT],
                         log_entries[msg_id][(int)EntryId::CPU_CHECKPOINT]); //Cache latency
            replacement_latency = writeLatency(report_files[core_id], log_entries[msg_id][(int)EntryId::REPLACE_ACTION_CHECKPOINT],
                                               log_entries[msg_id][(int)EntryId::CPU_CHECKPOINT]); //Replacement Overhead = 0

            request_latency = writeLatency(report_files[core_id], log_entries[msg_id], EntryId::REQ_BUS_CHECKPOINT); //Request Bus Latency
            worst_case_req_bus_latency[core_id] = max(request_latency, worst_case_req_bus_latency[core_id]);
        }
        else
        {
            writeLatency(report_files[core_id], log_entries[msg_id][(int)EntryId::REPLACE_ACTION_CHECKPOINT],
                         log_entries[msg_id][(int)EntryId::CPU_CHECKPOINT]); //Cache latency
            replacement_latency = writeLatency(report_files[core_id], log_entries[msg_id][(int)EntryId::CACHE_CHECKPOINT] + replacement_correction,
                                               log_entries[msg_id][(int)EntryId::REPLACE_ACTION_CHECKPOINT]); //Replacement

            request_latency = writeLatency(report_files[core_id], log_entries[msg_id][(int)EntryId::REQ_BUS_CHECKPOINT],
                                           log_entries[msg_id][(int)EntryId::CACHE_CHECKPOINT] + replacement_correction); //Request Bus Latency
            worst_case_req_bus_latency[core_id] = max(request_latency, worst_case_req_bus_latency[core_id]);
        }

        respond_latency = writeLatency(report_files[core_id], log_entries[msg_id], EntryId::RESP_BUS_CHECKPOINT); //Response Bus Latency
        worst_case_resp_bus_latency[core_id] = max(respond_latency, worst_case_resp_bus_latency[core_id]);

        writeLatency(report_files[core_id], log_entries[msg_id], EntryId::CPU_RX_CHECKPOINT); //Controller Post Response Latency

        report_files[core_id] << request_latency + respond_latency << ","; //Bus Latency
        worst_bus_latency[core_id] = max(request_latency + respond_latency, worst_bus_latency[core_id]);

        returned_latency = writeLatency(report_files[core_id], log_entries[msg_id][(int)EntryId::CPU_RX_CHECKPOINT],
                                        log_entries[msg_id][(int)EntryId::CPU_CHECKPOINT]); //Total Latency
        worst_case_latency[core_id] = max(returned_latency, worst_case_latency[core_id]);

        returned_latency = writeLatency(report_files[core_id], log_entries[msg_id][(int)EntryId::CPU_RX_CHECKPOINT],
                                        (max(this->last_checkpoint[core_id], log_entries[msg_id][(int)EntryId::CPU_CHECKPOINT]) +
                                         replacement_latency)); //Effective Latency
        max_effective_latency[core_id] = max(returned_latency, max_effective_latency[core_id]);
        average_latency[core_id] += returned_latency;
        num_request[core_id]++;

        report_files[core_id] << endl;
        this->last_checkpoint[core_id] = log_entries[msg_id][(int)EntryId::CPU_RX_CHECKPOINT];

        delete[] log_entries[msg_id];
        log_entries.erase(msg_id);
    }

    void Logger::registerReportPath(string file_path)
    {
        this->report_file_path = file_path;
    }

    void Logger::prepareReportFile(uint64_t core_id)
    {
        if (!this->report_files[core_id].is_open())
        {
            this->report_files[core_id].open(report_file_path + string("/LatencyReport_C") + to_string(core_id) + string(".csv"));
            this->report_files[core_id] << "RequstID,Request Address,Trace Cycle,";
            this->report_files[core_id] << "CPU Latency, Cache Controller Latency, Replacement Overhead, Requst Bus Latency,";
            this->report_files[core_id] << "Response Bus Latency,Controller Post Response Latency,";
            this->report_files[core_id] << "Bus Latency, Total Latency,Effective Latency" << endl;
        }
    }

    void Logger::initializeStats(uint64_t core_id)
    {
        if (worst_case_req_bus_latency.find(core_id) != worst_case_req_bus_latency.end())
            return;
        else
        {
            worst_case_req_bus_latency[core_id] = 0;
            worst_case_resp_bus_latency[core_id] = 0;
            worst_bus_latency[core_id] = 0;
            worst_case_replace_latency[core_id] = 0;
            worst_case_latency_minus_replace[core_id] = 0;
            worst_case_latency[core_id] = 0;
            max_effective_latency[core_id] = 0;
            average_latency[core_id] = 0;
            num_request[core_id] = 0;
            last_checkpoint[core_id] = 0;
        }
    }

    uint64_t Logger::writeLatency(ofstream &file_handler, uint64_t *checkpoints, EntryId idx)
    {
        if (checkpoints[(int)idx] == 0)
            file_handler << 0 << ",";
        else
        {
            for (int i = (int)idx - 1; i >= 0; i--)
            {
                if (checkpoints[i] != 0)
                    return writeLatency(file_handler, checkpoints[(int)idx], checkpoints[i]);
            }
        }
        return 0;
    }

    uint64_t Logger::writeLatency(ofstream &file_handler, uint64_t checkpoint2, uint64_t checkpoint1)
    {
        uint64_t diff = ((((int64_t)checkpoint2 - (int64_t)checkpoint1) > 0) ? checkpoint2 - checkpoint1 : 0);
        file_handler << diff << ",";
        return diff;
    }

    void Logger::traceEnd(uint64_t core_id)
    {
        stringstream stream;

        stream << "Worst-case Requst Bus Latency,";
        stream << "Worst-case Response Bus Latency,";
        stream << "Worst-case Bus Latency,";
        stream << "Worst-case Total Latency,";
        stream << "Worst-case Effective Latency,";
        stream << "Average Latency";

        report_files[core_id] << endl;
        report_files[core_id] << endl;
        report_files[core_id] << endl;
        report_files[core_id] << stream.str() << endl;

        if (!this->summary_file.is_open())
        {
            summary_file.open(report_file_path + string("/Summary.csv"));
            summary_file << "Core Id,";
            summary_file << stream.str();
            summary_file << ",Finish Cycle" << endl;
        }

        stream.str("");
        stream << worst_case_req_bus_latency[core_id] << ",";
        stream << worst_case_resp_bus_latency[core_id] << ",";
        stream << worst_bus_latency[core_id] << ",";
        stream << worst_case_latency[core_id] << ",";
        stream << max_effective_latency[core_id] << ",";
        stream << 1.0 * average_latency[core_id] / num_request[core_id];

        report_files[core_id] << stream.str() << endl;
        report_files[core_id].close();
        report_files.erase(core_id);

        summary_file << core_id << ",";
        summary_file << stream.str();
        summary_file << "," << last_cycle[core_id] << endl;
        last_cycle.erase(core_id);

        if (report_files.empty())
            summary_file.close();
    }

    void Logger::setClkCount(uint64_t core_id, uint64_t clk)
    {
        core_clk_count[core_id] = clk;
    }

    void Logger::setReplacementCorrection(uint64_t cycles)
    {
        this->replacement_correction = cycles;
    }
}