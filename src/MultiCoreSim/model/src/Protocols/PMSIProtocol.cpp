/*
 * File  :      PMSIProtocol.cpp
 * Author:      Mohammed Ismail
 * Email :      ismaim22@mcmaster.ca
 *
 * Created On Dec 11, 2021
 */

#include "../../header/Protocols/PMSIProtocol.h"
using namespace std;

namespace ns3
{
    PMSIProtocol::PMSIProtocol(GenericCache *privCache, const string &fsm_path, int coreId, int sharedMemId, int reqWbRatio, bool cache2Cache)
        : MSIProtocol(privCache, fsm_path, coreId, sharedMemId, reqWbRatio, cache2Cache)
    {
    }

    PMSIProtocol::~PMSIProtocol()
    {
    }

    vector<ControllerAction> &PMSIProtocol::handleAction(vector<int> &actions, Message &msg,
                                                         const GenericCache::CacheLineInfo &cache_line_info, int next_state)
    {
        bool putm_non_demanding = false;
        for (int i = 0; i < (int)actions.size(); i++)
        {
            if (actions[i] == (int)ActionId::PutM_nonDem)
            {
                actions.erase(actions.begin() + i);
                putm_non_demanding = true;
                break;
            }
        }
        MSIProtocol::handleAction(actions, msg, cache_line_info, next_state);

        if (putm_non_demanding == true)
        {
            ControllerAction controller_action;
            //send Bus request
            controller_action.type = ControllerAction::Type::SEND_BUS_MSG;
            controller_action.data = (void *)new Message{.msg_id = msg.msg_id,
                                                         .addr = msg.addr,
                                                         .complementary_value = (uint16_t)ActionId::PutM_nonDem,
                                                         .requestor_id = (uint16_t)this->m_shared_memory_id,
                                                         .responder_id = (uint16_t)this->m_core_id};

            this->controller_actions.push_back(controller_action);
        }
        return this->controller_actions;
    }
}