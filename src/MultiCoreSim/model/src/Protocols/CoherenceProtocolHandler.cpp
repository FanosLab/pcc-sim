/*
 * File  :      CoherenceProtocolHandler.cpp
 * Author:      Mohammed Ismail
 * Email :      ismaim22@mcmaster.ca
 *
 * Created On June 23, 2021
 */

#include "../../header/Protocols/CoherenceProtocolHandler.h"
using namespace std;

namespace ns3
{
    CoherenceProtocolHandler::CoherenceProtocolHandler(GenericCache *cache, const string &fsm_path, int coreId, int sharedMemId, int reqWbRatio, bool cache2Cache)
    {
        this->m_cache = cache;

        this->m_fsm = new FSMReader(fsm_path);

        this->m_core_id = coreId;
        this->m_shared_memory_id = sharedMemId;
        this->m_reqWbRatio = reqWbRatio;
        this->m_cache2Cache = cache2Cache;
    }

    CoherenceProtocolHandler::~CoherenceProtocolHandler()
    {
        delete this->m_fsm;
    }

    void CoherenceProtocolHandler::initializeCacheStates()
    { //This function should be overridden if the name
        //of the invalid state is different than "I"
        int initState = this->m_fsm->getState(string("I"));
        m_cache->InitalizeCacheStates(initState);
    }
}