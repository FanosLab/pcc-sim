/*
 * File  :      MSIProtocol.cpp
 * Author:      Mohammed Ismail
 * Email :      ismaim22@mcmaster.ca
 *
 * Created On June 23, 2021
 */

#include "../../header/Protocols/MSIProtocol.h"
using namespace std;

namespace ns3
{
    MSIProtocol::MSIProtocol(GenericCache *privCache, const string &fsm_path, int coreId, int sharedMemId, int reqWbRatio, bool cache2Cache) : CoherenceProtocolHandler(privCache, fsm_path, coreId, sharedMemId, reqWbRatio, cache2Cache)
    {
    }

    MSIProtocol::~MSIProtocol()
    {
    }

    FRFCFS_State MSIProtocol::getRequestState(const Message &msg, FRFCFS_State req_state)
    {
        GenericCache::CacheLineInfo cache_line_info = m_cache->GetCacheLineInfo(msg.addr);

        if (this->m_fsm->isStall(cache_line_info.state, msg.complementary_value))
            return FRFCFS_State::NonReady;
        else if (cache_line_info.IsExist || !cache_line_info.IsSetFull)
            return FRFCFS_State::Ready;

        if (req_state == FRFCFS_State::Waiting) //replacement request is previously issued
            return FRFCFS_State::Waiting;

        int candidate_way_index = this->m_cache->GetReplacementLine(cache_line_info.set_idx); //ToDo: modify this function to return only lines in stable state
        if (candidate_way_index == -1)
            return FRFCFS_State::NonReady;
        else
        {
            GenericCache::CacheLineInfo candidate_line_info =
                m_cache->GetCacheLineInfo(this->m_cache->CpuPhyAddr(cache_line_info.set_idx, candidate_way_index));
            if (candidate_line_info.state == this->m_fsm->getState(string("M")))
                return FRFCFS_State::NeedsAction;
            else if (!this->m_fsm->isStable(candidate_line_info.state))
                return FRFCFS_State::NonReady;
            else
            {
                //invalidate the candidate line (this happens only to states that does't require Put message i.e. S state)
                GenericCacheFrmt candidate_line =
                    this->m_cache->ReadCacheLine(cache_line_info.set_idx, candidate_way_index);

                candidate_line.valid = false;
                candidate_line.state = this->m_fsm->getState(string("I"));
                this->m_cache->WriteCacheLine(cache_line_info.set_idx, candidate_way_index, candidate_line);
                return FRFCFS_State::Ready;
            }
        }
    }

    const vector<ControllerAction> &MSIProtocol::processRequest(Message &request_msg)
    {
        GenericCache::CacheLineInfo cache_line_info = m_cache->GetCacheLineInfo(request_msg.addr);
        Message *replacement_msg = NULL;
        if (cache_line_info.IsExist == false && request_msg.source == Message::Source::LOWER_INTERCONNECT)
            handleNonExistingLine(cache_line_info, &replacement_msg);

        EventId event_id;
        int next_state;
        vector<int> actions;

        this->readEvent((replacement_msg == NULL) ? request_msg : *replacement_msg, &event_id);
        this->m_fsm->getTransition(cache_line_info.state, (int)event_id, next_state, actions);

        if (replacement_msg == NULL)
            return handleAction(actions, request_msg, cache_line_info, next_state);
        else
        {
            handleAction(actions, *replacement_msg, cache_line_info, next_state);
            delete replacement_msg;

            Logger::getLogger()->updateRequest(request_msg.msg_id, Logger::EntryId::REPLACE_ACTION_CHECKPOINT); //Add replacement checkpoint

            return this->controller_actions;
        }
    }

    vector<ControllerAction> &MSIProtocol::handleAction(vector<int> &actions, Message &msg,
                                                        const GenericCache::CacheLineInfo &cache_line_info, int next_state)
    {
        this->controller_actions.clear();
        for (int action : actions)
        {
            ControllerAction controller_action;
            switch (static_cast<ActionId>(action))
            {
            case ActionId::Stall: //return request to processing queue
                controller_action.type = ControllerAction::Type::ADD_PENDING;
                controller_action.data = (void *)new Message;
                ((Message *)controller_action.data)->copy(msg);
                break;

            case ActionId::Hit: //remove request from pending and respond to cpu, update cache line
                controller_action.type = (msg.source == Message::Source::LOWER_INTERCONNECT)
                                             ? ControllerAction::Type::HIT_Action
                                             : ControllerAction::Type::REMOVE_PENDING;
                controller_action.data = (void *)new Message;
                ((Message *)controller_action.data)->copy(msg);
                break;

            case ActionId::GetS:
            case ActionId::GetM:
                //add request to pending requests
                controller_action.type = ControllerAction::Type::ADD_PENDING;
                controller_action.data = (void *)new Message;
                ((Message *)controller_action.data)->copy(msg);
                this->controller_actions.push_back(controller_action);

                //send Bus request, update cache line
                controller_action.type = ControllerAction::Type::SEND_BUS_MSG;
                controller_action.data = (void *)new Message{.msg_id = msg.msg_id,
                                                             .addr = msg.addr,
                                                             .complementary_value = (uint16_t)action,
                                                             .requestor_id = (uint16_t)this->m_core_id,
                                                             .responder_id = (uint16_t)this->m_shared_memory_id};
                break;
            case ActionId::PutM:
                //send Bus request, update cache line
                controller_action.type = ControllerAction::Type::SEND_BUS_MSG;
                controller_action.data = (void *)new Message{.msg_id = msg.msg_id,
                                                             .addr = msg.addr,
                                                             .complementary_value = (uint16_t)action,
                                                             .requestor_id = (uint16_t)this->m_shared_memory_id,
                                                             .responder_id = (uint16_t)this->m_core_id};
                break;

            case ActionId::Data2Req:
            case ActionId::Data2Both:
                //Do writeback, update cache line
                controller_action.type = ControllerAction::Type::WRITE_BACK;
                controller_action.data = (void *)new Message{.msg_id = msg.msg_id,
                                                             .addr = msg.addr,
                                                             .complementary_value = (uint16_t)((action == (int)ActionId::Data2Both) ? 1 : 0),
                                                             .requestor_id = (uint16_t)msg.requestor_id,
                                                             .responder_id = (uint16_t)this->m_core_id};
                break;

            case ActionId::SaveReq:
                //send Bus request, update cache line
                controller_action.type = ControllerAction::Type::SAVE_REQ_FOR_WRITE_BACK;
                controller_action.data = (void *)new Message{.msg_id = msg.msg_id,
                                                             .addr = msg.addr,
                                                             .requestor_id = (uint16_t)msg.requestor_id,
                                                             .responder_id = (uint16_t)this->m_core_id};
                break;

            case ActionId::Fault:
                std::cout << " MSIProtocol: Fault Transaction detected" << std::endl;
                exit(0);
                break;
            }

            this->controller_actions.push_back(controller_action);
        }

        //update cache line
        if ((actions.empty() && cache_line_info.IsExist) ||
            (!actions.empty() && actions[0] != (int)ActionId::Stall))
        {
            GenericCacheFrmt cache_line(next_state, this->m_fsm->isValidState(next_state),
                                        m_cache->CpuAddrMap(msg.addr).tag, msg.data);
            delete[] msg.data;

            ControllerAction controller_action;
            controller_action.type = ControllerAction::Type::UPDATE_CACHE_LINE;
            controller_action.data = (void *)new uint8_t[sizeof(cache_line) + sizeof(cache_line_info.set_idx) + sizeof(cache_line_info.way_idx)];
            memcpy(controller_action.data, &cache_line, sizeof(cache_line));
            memcpy((uint8_t *)controller_action.data + sizeof(cache_line), &cache_line_info.set_idx, sizeof(cache_line_info.set_idx));
            memcpy((uint8_t *)controller_action.data + sizeof(cache_line) + sizeof(cache_line_info.set_idx),
                   &cache_line_info.way_idx, sizeof(cache_line_info.way_idx));

            this->controller_actions.push_back(controller_action);
        }

        return this->controller_actions;
    }

    void MSIProtocol::readEvent(Message &msg, EventId *out_id)
    {
        switch (msg.source)
        {
        case Message::Source::LOWER_INTERCONNECT:
            *out_id = (msg.complementary_value == CpuFIFO::REQTYPE::READ) ? EventId::Load : (msg.complementary_value == CpuFIFO::REQTYPE::WRITE) ? EventId::Store
                                                                                                                                                 : EventId::Replacement;
            return;

        case Message::Source::UPPER_INTERCONNECT:
            if (msg.data != NULL)
                *out_id = EventId::OwnData;

            else
            {
                switch (msg.complementary_value)
                {
                case SNOOPPrivCohTrans::GetSTrans:
                    *out_id = (msg.requestor_id == m_core_id) ? EventId::Own_GetS : EventId::Other_GetS;
                    return;
                case SNOOPPrivCohTrans::GetMTrans:
                    *out_id = (msg.requestor_id == m_core_id) ? EventId::Own_GetM : EventId::Other_GetM;
                    return;
                case SNOOPPrivCohTrans::PutMTrans:
                    *out_id = (msg.responder_id == m_core_id) ? EventId::Own_PutM : EventId::Other_PutM;
                    return;
                default: // Invalid Transaction
                    std::cout << " MSIProtocol: Invalid Transaction detected on the Bus" << std::endl;
                    exit(0);
                }
            }
            return;
        }
    }

    void MSIProtocol::handleNonExistingLine(GenericCache::CacheLineInfo &cache_line_info, Message **msg)
    {
        int target_way_index;
        if (cache_line_info.IsSetFull == true)
        {
            *msg = new Message{.msg_id = 0,
                               .complementary_value = CpuFIFO::REQTYPE::REPLACE,
                               .requestor_id = (uint16_t)this->m_core_id,
                               .responder_id = (uint16_t)this->m_shared_memory_id,
                               .source = Message::Source::LOWER_INTERCONNECT};

            target_way_index = this->m_cache->GetReplacementLine(cache_line_info.set_idx);
            (*msg)->addr = this->m_cache->CpuPhyAddr(cache_line_info.set_idx, target_way_index);
        }
        else
        {
            *msg = NULL;
            target_way_index = m_cache->GetEmptyCacheLine(cache_line_info.set_idx);
            if (target_way_index == -1)
            {
                std::cout << "Coherence Protocol: Fault non-empty Set condition !!!" << std::endl;
                exit(0);
            }
        }

        GenericCacheFrmt cache_line = this->m_cache->ReadCacheLine(cache_line_info.set_idx, target_way_index);
        cache_line_info.way_idx = target_way_index;
        cache_line_info.state = cache_line.state;
    }
}