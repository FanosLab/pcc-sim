/*
 * File  :      MESIProtocol.cpp
 * Author:      Mohammed Ismail
 * Email :      ismaim22@mcmaster.ca
 *
 * Created On Nov 28, 2021
 */

#include "../../header/Protocols/MESIProtocol.h"
using namespace std;

namespace ns3
{
    MESIProtocol::MESIProtocol(GenericCache *privCache, const string &fsm_path, int coreId, int sharedMemId, int reqWbRatio, bool cache2Cache) : MSIProtocol(privCache, fsm_path, coreId, sharedMemId, reqWbRatio, cache2Cache)
    {
    }

    MESIProtocol::~MESIProtocol()
    {
    }

    FRFCFS_State MESIProtocol::getRequestState(const Message &msg, FRFCFS_State req_state)
    {
        GenericCache::CacheLineInfo cache_line_info = m_cache->GetCacheLineInfo(msg.addr);
        
        if (this->m_fsm->isStall(cache_line_info.state, msg.complementary_value))
            return FRFCFS_State::NonReady;
        else if (cache_line_info.IsExist || !cache_line_info.IsSetFull)
            return FRFCFS_State::Ready;

        if (req_state == FRFCFS_State::Waiting) //replacement request is previously issued
            return FRFCFS_State::Waiting;

        int candidate_way_index = this->m_cache->GetReplacementLine(cache_line_info.set_idx); //ToDo: modify this function to return only lines in stable state
        if (candidate_way_index == -1)
            return FRFCFS_State::NonReady;
        else
        {
            GenericCache::CacheLineInfo candidate_line_info =
                m_cache->GetCacheLineInfo(this->m_cache->CpuPhyAddr(cache_line_info.set_idx, candidate_way_index));
            if (candidate_line_info.state == this->m_fsm->getState(string("M")) ||
                candidate_line_info.state == this->m_fsm->getState(string("E")))
                return FRFCFS_State::NeedsAction;
            else if (!this->m_fsm->isStable(candidate_line_info.state))
                return FRFCFS_State::NonReady;
            else
            {
                //invalidate the candidate line (this happens only to states that does't require Put message i.e. S state)
                GenericCacheFrmt candidate_line =
                    this->m_cache->ReadCacheLine(cache_line_info.set_idx, candidate_way_index);

                candidate_line.valid = false;
                candidate_line.state = this->m_fsm->getState(string("I"));
                this->m_cache->WriteCacheLine(cache_line_info.set_idx, candidate_way_index, candidate_line);
                return FRFCFS_State::Ready;
            }
        }
    }

    void MESIProtocol::readEvent(Message &msg, MSIProtocol::EventId *out_id)
    {
        MSIProtocol::readEvent(msg, out_id);

        if (*out_id == MSIProtocol::EventId::OwnData)
            *out_id = (MSIProtocol::EventId)((msg.complementary_value == 2) ? EventId::OwnData_Execlusive : EventId::OwnData);
    }
}