/*
 * File  :      LLCMSIProtocol.cpp
 * Author:      Mohammed Ismail
 * Email :      ismaim22@mcmaster.ca
 *
 * Created On July 25, 2021
 */

#include "../../header/Protocols/LLCMSIProtocol.h"
using namespace std;

namespace ns3
{
    LLCMSIProtocol::LLCMSIProtocol(GenericCache *privCache, const string &fsm_path, int coreId) : CoherenceProtocolHandler(privCache, fsm_path, coreId, -1, 0, false)
    {
    }

    LLCMSIProtocol::~LLCMSIProtocol()
    {
    }

    FRFCFS_State LLCMSIProtocol::getRequestState(const Message &msg, FRFCFS_State req_state)
    {
        GenericCache::CacheLineInfo cache_line_info = m_cache->GetCacheLineInfo(msg.addr);

        if (this->m_fsm->isStable(cache_line_info.state))
            return FRFCFS_State::Ready;
        else if(msg.data != NULL)
            return FRFCFS_State::Ready;
        else if (req_state == FRFCFS_State::Waiting) //replacement request is previously issued
            return FRFCFS_State::Waiting;
        else
            return FRFCFS_State::NonReady;


        int candidate_way_index = this->m_cache->GetReplacementLine(cache_line_info.set_idx); //ToDo: modify this function to return only lines in stable state
        if (candidate_way_index == -1)
            return FRFCFS_State::NonReady;
        else
        {
            GenericCache::CacheLineInfo candidate_line_info =
                m_cache->GetCacheLineInfo(this->m_cache->CpuPhyAddr(cache_line_info.set_idx, candidate_way_index));
            if (!this->m_fsm->isStable(candidate_line_info.state))
                return FRFCFS_State::NonReady;
            else
                return FRFCFS_State::NeedsAction;
        }
    }

    const vector<ControllerAction> &LLCMSIProtocol::processRequest(Message &request_msg)
    {
        GenericCache::CacheLineInfo cache_line_info = m_cache->GetCacheLineInfo(request_msg.addr);
        Message *replacement_msg = NULL;
        if (cache_line_info.IsExist == false && request_msg.source == Message::Source::LOWER_INTERCONNECT)
            handleNonExistingLine(cache_line_info, &replacement_msg);

        EventId event_id;
        int next_state;
        vector<int> actions;
        
        this->readEvent((replacement_msg == NULL) ? request_msg : *replacement_msg, cache_line_info, &event_id);
        this->m_fsm->getTransition(cache_line_info.state, (int)event_id, next_state, actions);

        if (replacement_msg == NULL)
            return handleAction(actions, request_msg, cache_line_info, next_state);
        else
        {
            handleAction(actions, *replacement_msg, cache_line_info, next_state);
            delete replacement_msg;

            return this->controller_actions;
        }
    }

    vector<ControllerAction> &LLCMSIProtocol::handleAction(vector<int> &actions, Message &msg,
                                                           const GenericCache::CacheLineInfo &cache_line_info, int next_state)
    {
        int line_owner_id = cache_line_info.owner_id;
        this->controller_actions.clear();

        for (int action : actions)
        {
            ControllerAction controller_action;
            switch (static_cast<ActionId>(action))
            {
            case ActionId::Stall: //return request to processing queue
                controller_action.type = ControllerAction::Type::ADD_PENDING;
                controller_action.data = (void *)new Message;
                ((Message*)controller_action.data)->copy(msg);
                break;

            case ActionId::SendData: //remove request from pending and respond to request
                controller_action.type = ControllerAction::Type::REMOVE_PENDING;
                controller_action.data = (void *)new Message;
                ((Message*)controller_action.data)->copy(msg);
                ((Message*)controller_action.data)->complementary_value = 0; //DualTrans == false
                break;

            case ActionId::GetData:
                //add request to pending requests
                controller_action.type = ControllerAction::Type::ADD_PENDING;
                controller_action.data = (void *)new Message;
                ((Message*)controller_action.data)->copy(msg);
                this->controller_actions.push_back(controller_action);

                //send Bus request, update cache line
                controller_action.type = ControllerAction::Type::SEND_BUS_MSG;
                controller_action.data = (void *)new Message{.msg_id = msg.msg_id,
                                                             .addr = msg.addr,
                                                             .complementary_value = (uint16_t)action,
                                                             .requestor_id = (uint16_t)this->m_core_id,
                                                             .responder_id = (uint16_t)this->m_shared_memory_id};
                break;

            case ActionId::SetOwner:
                line_owner_id = (msg.data == NULL) ? msg.requestor_id : msg.responder_id;
                controller_action.type = ControllerAction::Type::NO_ACTION;
                break;
            case ActionId::ClearOwner:
                line_owner_id = -1;
                controller_action.type = ControllerAction::Type::NO_ACTION;
                break;

            case ActionId::SaveData: //update cacheline (it happens by default if Action is not Stall)
                controller_action.type = ControllerAction::Type::NO_ACTION;
                break;

            case ActionId::IssueInv:
                //send Bus request, update cache line
                controller_action.type = ControllerAction::Type::SEND_BUS_MSG;
                controller_action.data = (void *)new Message{.msg_id = msg.msg_id,
                                                             .addr = msg.addr,
                                                             .complementary_value = (uint16_t)action,
                                                             .requestor_id = (uint16_t)this->m_shared_memory_id,
                                                             .responder_id = (uint16_t)this->m_core_id};
                break;

            case ActionId::WriteBack:
                //Do writeback, update cache line
                controller_action.type = ControllerAction::Type::WRITE_BACK;
                controller_action.data = (void *)new Message{.msg_id = msg.msg_id,
                                                             .addr = msg.addr,
                                                             .complementary_value = 0,
                                                             .requestor_id = (uint16_t)msg.requestor_id,
                                                             .responder_id = (uint16_t)this->m_core_id};
                break;

            case ActionId::Fault:
                std::cout << " LLCMSIProtocol: Fault Transaction detected" << std::endl;
                exit(0);
                break;
            }

            this->controller_actions.push_back(controller_action);
        }

        //update cache line
        if ((actions.empty() && cache_line_info.IsExist) ||
            (!actions.empty() && actions[0] != (int)ActionId::Stall))
        {
            GenericCacheFrmt cache_line(next_state, this->m_fsm->isValidState(next_state),
                                        m_cache->CpuAddrMap(msg.addr).tag, msg.data, line_owner_id);
            delete[] msg.data;

            ControllerAction controller_action;
            controller_action.type = ControllerAction::Type::UPDATE_CACHE_LINE;
            controller_action.data = (void *)new uint8_t[sizeof(cache_line) + sizeof(cache_line_info.set_idx) + sizeof(cache_line_info.way_idx)];
            memcpy(controller_action.data, &cache_line, sizeof(cache_line));
            memcpy((uint8_t *)controller_action.data + sizeof(cache_line), &cache_line_info.set_idx, sizeof(cache_line_info.set_idx));
            memcpy((uint8_t *)controller_action.data + sizeof(cache_line) + sizeof(cache_line_info.set_idx),
                   &cache_line_info.way_idx, sizeof(cache_line_info.way_idx));

            this->controller_actions.push_back(controller_action);
        }

        return this->controller_actions;
    }

    void LLCMSIProtocol::readEvent(Message &msg, GenericCache::CacheLineInfo cache_line_info, EventId *out_id)
    {
        switch (msg.source)
        {
        case Message::Source::UPPER_INTERCONNECT:
            if (msg.data != NULL)
                *out_id = EventId::Data_fromUpperInterface;
            break;

        case Message::Source::LOWER_INTERCONNECT:
            if (msg.data != NULL)
                *out_id = EventId::Data_fromLowerInterface;
                
            else
            {
                switch (msg.complementary_value)
                {
                case SNOOPPrivCohTrans::GetSTrans:
                    *out_id = EventId::GetS;
                    break;
                case SNOOPPrivCohTrans::GetMTrans:
                    *out_id = EventId::GetM;
                    break;
                case SNOOPPrivCohTrans::PutMTrans:
                    *out_id = (msg.requestor_id == cache_line_info.owner_id || msg.responder_id == cache_line_info.owner_id) ? EventId::PutM_fromOwner : EventId::PutM_fromNonOwner;
                    break;
                default: // Invalid Transaction
                    std::cout << " LLCMSIProtocol: Invalid Transaction detected on the Bus" << std::endl;
                    exit(0);
                }
            }
            break;
        }
    }

    void LLCMSIProtocol::handleNonExistingLine(GenericCache::CacheLineInfo &cache_line_info, Message **msg)
    {
        int target_way_index;
        if (cache_line_info.IsSetFull == true)
        {
            *msg = new Message{.msg_id = 0,
                               .complementary_value = CpuFIFO::REQTYPE::REPLACE,
                               .requestor_id = (uint16_t)this->m_core_id,
                               .responder_id = (uint16_t)this->m_shared_memory_id,
                               .source = Message::Source::LOWER_INTERCONNECT};

            target_way_index = this->m_cache->GetReplacementLine(cache_line_info.set_idx);
            (*msg)->addr = this->m_cache->CpuPhyAddr(cache_line_info.set_idx, target_way_index);
        }
        else
        {
            *msg = NULL;
            target_way_index = m_cache->GetEmptyCacheLine(cache_line_info.set_idx);
            if (target_way_index == -1)
            {
                std::cout << "Coherence Protocol: Fault non-empty Set condition !!!" << std::endl;
                exit(0);
            }
        }

        GenericCacheFrmt cache_line = this->m_cache->ReadCacheLine(cache_line_info.set_idx, target_way_index);
        cache_line_info.way_idx = target_way_index;
        cache_line_info.state = cache_line.state;
    }

    void LLCMSIProtocol::createDefaultCacheLine(uint64_t address, GenericCacheFrmt *cache_line)
    {
        int state = this->m_fsm->getState(string("IorS"));
        GenericCacheFrmt default_line(state, this->m_fsm->isValidState(state),
                                    m_cache->CpuAddrMap(address).tag, NULL);
        //memcpy(cache_line, &default_line, sizeof(GenericCacheFrmt));
        cache_line->copy(default_line);
    }
}