/*
 * File  :      MCoreSimProject.cc
 * Author:      Salah Hessien
 * Email :      salahga@mcmaster.ca
 *
 * Created On February 15, 2020
 */

#include "../header/MCoreSimProject.h"
#include "ns3/simulator.h"

using namespace std;
using namespace ns3;

/*
 * Create the MCoreSimProject for the supplied configuration data
 */

MCoreSimProject::MCoreSimProject(MCoreSimProjectXml projectXmlCfg)
{

  // Set the project xml
  m_projectXmlCfg = projectXmlCfg;

  // Get clock frequency
  m_dt = projectXmlCfg.GetBusClkInNanoSec();
  m_busCycle = 0;

  // Get Run Till Sim End Flag
  m_runTillSimEnd = projectXmlCfg.GetRunTillSimEnd();

  // Get Simulation time to run
  m_totalTimeInSeconds = (m_runTillSimEnd == true) ? std::numeric_limits<int>::max() : projectXmlCfg.GetTotalTimeInSeconds();

  // Enable Log File Generation
  m_logFileGenEnable = projectXmlCfg.GetLogFileGenEnable();

  // initialize Simulator components
  m_cpuCoreGens = list<Ptr<CpuCoreGenerator>>();
  m_cpuFIFO = list<CpuFIFO *>();
  m_cpuCacheCtrl = list<Ptr<CacheController>>();
  m_busIfFIFO = list<BusIfFIFO *>();

  // Get all cpu configurations from xml
  list<CacheXml> xmlPrivateCaches = projectXmlCfg.GetPrivateCaches();

  CacheXml xmlSharedCache = projectXmlCfg.GetSharedCache();

  // Get L1Bus configurations
  L1BusCnfgXml L1BusCnfg = projectXmlCfg.GetL1BusCnfg();

  m_fsm_protocol_path = string("/Users/Mhossam/Documents/PhD_Work/CacheSim_Stable/cachesim/Protocols_FSM/");
  m_fsm_llc_protocol_path = string("/Users/Mhossam/Documents/PhD_Work/CacheSim_Stable/cachesim/Protocols_FSM/");
  // Get Coherence protocol type
  GetCohrProtocolType();

  m_maxPendReq = 0;

  // iterate over each core
  for (list<CacheXml>::iterator it = xmlPrivateCaches.begin(); it != xmlPrivateCaches.end(); it++)
  {
    CacheXml PrivateCacheXml = *it;

    /* 
      * instantiate cpu FIFOs
     */
    CpuFIFO *newCpuFIFO = new CpuFIFO(PrivateCacheXml.GetCacheId(), projectXmlCfg.GetCpuFIFOSize());
    m_cpuFIFO.push_back(newCpuFIFO);

    /* 
      * instantiate cpu cores
     */
    Ptr<CpuCoreGenerator> newCpuCore = CreateObject<CpuCoreGenerator>(newCpuFIFO);
    stringstream bmTraceFile, cpuTraceFile, ctrlTraceFile;
    bmTraceFile << projectXmlCfg.GetBMsPath() << "/trace_C" << PrivateCacheXml.GetCacheId() << ".trc.shared";
    cpuTraceFile << projectXmlCfg.GetBMsPath() << "/" << projectXmlCfg.GetCpuTraceFile() << PrivateCacheXml.GetCacheId() << ".txt";
    ctrlTraceFile << projectXmlCfg.GetBMsPath() << "/" << projectXmlCfg.GetCohCtrlsTraceFile() << PrivateCacheXml.GetCacheId() << ".txt";
    double cpuClkPeriod = PrivateCacheXml.GetCpuClkNanoSec();
    double cpuClkSkew = cpuClkPeriod * PrivateCacheXml.GetCpuClkSkew() / 100.00;
    newCpuCore->SetCoreId(PrivateCacheXml.GetCacheId());
    newCpuCore->SetBmFileName(bmTraceFile.str());
    newCpuCore->SetCpuTraceFile(cpuTraceFile.str());
    newCpuCore->SetCtrlsTraceFile(ctrlTraceFile.str());
    newCpuCore->SetDt(cpuClkPeriod);
    newCpuCore->SetClkSkew(cpuClkSkew);
    newCpuCore->SetLogFileGenEnable(m_logFileGenEnable);
    newCpuCore->SetOutOfOrderStages(projectXmlCfg.GetOutOfOrderStages());
    m_cpuCoreGens.push_back(newCpuCore);

    bm_paths.push_back(bmTraceFile.str());
    /* 
      * instantiate Cache Controller Bus IF FIFOs
     */
    BusIfFIFO *newBusIfFIFO;
    if (m_cohrProt == CohProtType::SNOOP_PMSI || m_cohrProt == CohProtType::SNOOP_PMESI)
      newBusIfFIFO = new BusIfFIFO_PMSI(PrivateCacheXml.GetCacheId(), projectXmlCfg.GetBusFIFOSize());
    else
      newBusIfFIFO = new BusIfFIFO(PrivateCacheXml.GetCacheId(), projectXmlCfg.GetBusFIFOSize());

    m_busIfFIFO.push_back(newBusIfFIFO);

    /*
      * instantiate cache controllers
      */
    Ptr<CacheController> newCacheCtrl = CreateObject<CacheController>(PrivateCacheXml,
                                                                      m_fsm_protocol_path,
                                                                      newBusIfFIFO, newCpuFIFO, projectXmlCfg.GetCache2Cache(),
                                                                      xmlSharedCache.GetCacheId(), m_cohrProt);
    m_cpuCacheCtrl.push_back(newCacheCtrl);

    if (m_maxPendReq < PrivateCacheXml.GetNPendReq())
    {
      m_maxPendReq = PrivateCacheXml.GetNPendReq();
    }
  }

  /* 
  * instantiate LLC/Shared Memory controller Bus IF FIFOs
  */
  m_sharedCacheBusIfFIFO = new BusIfFIFO(xmlSharedCache.GetCacheId(), projectXmlCfg.GetBusFIFOSize());

  /* 
  * instantiate LLC/Shared Memory -> DRAM controller Bus IF FIFOs
  */
  m_sharedCacheDRAMBusIfFIFO = new DRAMIfFIFO(projectXmlCfg.GetDRAMId(), projectXmlCfg.GetBusFIFOSize());

  /* 
  * instantiate LLC/Shared Memory controller
  */
  double ctrlClkPeriod = xmlSharedCache.GetCtrlClkNanoSec();
  double ctrlClkSkew = ctrlClkPeriod * xmlSharedCache.GetCtrlClkSkew() / 100.00;
  uint32_t cacheLines = xmlSharedCache.GetCacheSize() / xmlSharedCache.GetBlockSize();
  uint32_t nsets = cacheLines / xmlSharedCache.GetNWays();
  // ReplcPolicy L2ReplcPolicy = ReplcPolicyDecode(xmlSharedCache.GetReplcPolicy());
  m_SharedCacheCtrl = CreateObject<CacheController>(xmlSharedCache,
                                                    m_fsm_llc_protocol_path,
                                                    m_sharedCacheDRAMBusIfFIFO, m_sharedCacheBusIfFIFO,
                                                    projectXmlCfg.GetCache2Cache(), xmlSharedCache.GetCacheId(),
                                                    m_llcCohrProt);

  /* 
  * instantiate DRAM Controller
  */
  ctrlClkPeriod = projectXmlCfg.GetDRAMCtrlClkNanoSec();
  ctrlClkSkew = ctrlClkPeriod * projectXmlCfg.GetDRAMCtrlClkSkew() / 100.00;
  m_dramCtrl = CreateObject<DRAMCtrl>(m_sharedCacheDRAMBusIfFIFO);
  m_dramCtrl->SetDramFxdLatcy(projectXmlCfg.GetDRAMFixedLatcy());
  m_dramCtrl->SetDramModel(projectXmlCfg.GetDRAMModle());
  m_dramCtrl->SetDramOutstandReq(projectXmlCfg.GetDRAMOutstandReq());
  m_dramCtrl->SetMemCtrlId(projectXmlCfg.GetDRAMId());
  m_dramCtrl->SetDt(ctrlClkPeriod);
  m_dramCtrl->SetClkSkew(ctrlClkSkew);
  m_dramCtrl->SetLogFileGenEnable(m_logFileGenEnable);
  /* 
  * instantiate Interconnect FIFOs
  */
  m_interConnectFIFO = CreateObject<InterConnectFIFO>();
  m_interConnectFIFO->m_ReqMsgFIFO.SetFifoDepth(projectXmlCfg.GetBusFIFOSize());
  m_interConnectFIFO->m_RespMsgFIFO.SetFifoDepth(projectXmlCfg.GetBusFIFOSize());

  /* 
  * instantiate bus arbiter
  */
  m_busArbiter = CreateObject<BusArbiter>(m_busIfFIFO, m_sharedCacheBusIfFIFO, m_interConnectFIFO);
  m_busArbiter->SetCacheBlkSize(xmlSharedCache.GetBlockSize());
  m_busArbiter->SetDt(ctrlClkPeriod);
  m_busArbiter->SetClkSkew(ctrlClkSkew);
  m_busArbiter->SetNumPrivCore(projectXmlCfg.GetNumPrivCore());
  m_busArbiter->SetCache2Cache(projectXmlCfg.GetCache2Cache());
  m_busArbiter->SetNumReqCycles(L1BusCnfg.GetReqBusLatcy());
  m_busArbiter->SetNumRespCycles(L1BusCnfg.GetRespBusLatcy());
  m_busArbiter->SetIsWorkConserv(L1BusCnfg.GetWrkConservFlag());
  m_busArbiter->SetBusArchitecture(L1BusCnfg.GetBusArchitecture());
  m_busArbiter->SetBusArbitration(L1BusCnfg.GetBusArbitration());
  m_busArbiter->SetReqBusArb(L1BusCnfg.GetReqBusArb());
  m_busArbiter->SetRespBusArb(L1BusCnfg.GetRespBusArb());
  m_busArbiter->SetCohProtType(m_cohrProt);
  m_busArbiter->SetMaxPendingReq(m_maxPendReq);
  m_busArbiter->SetLogFileGenEnable(m_logFileGenEnable);

  /*
   * instantiate LatencyLogger
   */
  std::list<CpuFIFO *>::iterator CpuFIFO_itr = m_cpuFIFO.begin();
  std::list<BusIfFIFO *>::iterator PrivCacheFIFO_itr = m_busIfFIFO.begin();

  for (list<CacheXml>::iterator it = xmlPrivateCaches.begin(); it != xmlPrivateCaches.end(); it++)
  {
    CacheXml PrivateCacheXml = *it;
    CpuFIFO *cpuFIFO = (*CpuFIFO_itr);
    BusIfFIFO *privCachBusFIFO = (*PrivCacheFIFO_itr);

    double cpuClkPeriod = PrivateCacheXml.GetCpuClkNanoSec();
    double cpuClkSkew = cpuClkPeriod * PrivateCacheXml.GetCpuClkSkew() / 100.00;
    stringstream latencyTraceFile, latencyReportFile;
    latencyTraceFile << projectXmlCfg.GetBMsPath() << "/LatencyTrace_C" << PrivateCacheXml.GetCacheId() << ".trc";
    latencyReportFile << projectXmlCfg.GetBMsPath() << "/LatencyReport_C" << PrivateCacheXml.GetCacheId() << ".csv";

    uint32_t cacheLines = PrivateCacheXml.GetCacheSize() / PrivateCacheXml.GetBlockSize();
    uint32_t nsets = cacheLines / PrivateCacheXml.GetNWays();

    Ptr<LatencyLogger> newLatencyLogger = CreateObject<LatencyLogger>(m_interConnectFIFO, cpuFIFO, privCachBusFIFO);
    newLatencyLogger->SetDt(cpuClkPeriod / 2.0);
    newLatencyLogger->SetClkSkew(cpuClkSkew);
    newLatencyLogger->SetCoreId(PrivateCacheXml.GetCacheId());
    newLatencyLogger->SetSharedMemId(xmlSharedCache.GetCacheId());
    newLatencyLogger->SetOverSamplingRatio(2);
    newLatencyLogger->SetLatencyTraceFile(latencyTraceFile.str());
    newLatencyLogger->SetLatencyReprtFile(latencyReportFile.str());
    newLatencyLogger->SetPrivCacheNsets(nsets);
    newLatencyLogger->SetPrivCacheBlkSize(PrivateCacheXml.GetBlockSize());
    newLatencyLogger->SetLogFileGenEnable(m_logFileGenEnable);
    m_latencyLogger.push_back(newLatencyLogger);
    CpuFIFO_itr++;
    PrivCacheFIFO_itr++;
  }
  Logger::getLogger()->registerReportPath(projectXmlCfg.GetBMsPath() + string("/newLogger"));
  if (L1BusCnfg.GetReqBusArb() == "RR" ||
      L1BusCnfg.GetReqBusArb() == "WRR" ||
      L1BusCnfg.GetReqBusArb() == "HRR")
    Logger::getLogger()->setReplacementCorrection(L1BusCnfg.GetRespBusLatcy());
  else
    Logger::getLogger()->setReplacementCorrection(L1BusCnfg.GetRespBusLatcy());
  /* 
     * print configurations
     */
  if (m_logFileGenEnable)
  {
    cout << "\nSharedMemCtrl " << xmlSharedCache.GetCacheId() << ", Clk (NanoSec) = " << xmlSharedCache.GetCtrlClkNanoSec() << ", Clk-Skew = (ns) " << ctrlClkSkew << endl;
    cout << "	SharedMem: BlkSize (Bytes) = " << xmlSharedCache.GetBlockSize() << ", CacheSize (Bytes) = " << xmlSharedCache.GetCacheSize() << ", MappingType (0:direct, 1: associative) = " << xmlSharedCache.GetMappingType() << ", Nways = " << xmlSharedCache.GetNWays() << ", Nsets = " << nsets << endl;
  }
}

MCoreSimProject::~MCoreSimProject()
{
  for (CpuFIFO *cpuFIFO_ptr : m_cpuFIFO)
  {
    delete cpuFIFO_ptr;
  }
  m_cpuFIFO.clear();

  for (BusIfFIFO *busIfFIFO_ptr : m_busIfFIFO)
  {
    delete busIfFIFO_ptr;
  }
  m_busIfFIFO.clear();

  delete m_sharedCacheBusIfFIFO;
  delete m_sharedCacheDRAMBusIfFIFO;
}

/* 
 * start simulation engines
 */
void MCoreSimProject::Start()
{

  for (list<Ptr<CpuCoreGenerator>>::iterator it = m_cpuCoreGens.begin(); it != m_cpuCoreGens.end(); it++)
  {
    (*it)->init();
  }

  for (list<Ptr<CacheController>>::iterator it = m_cpuCacheCtrl.begin(); it != m_cpuCacheCtrl.end(); it++)
  {
    (*it)->init();
  }

  for (list<Ptr<LatencyLogger>>::iterator it = m_latencyLogger.begin(); it != m_latencyLogger.end(); it++)
  {
    (*it)->init();
  }

  m_SharedCacheCtrl->init();
  m_SharedCacheCtrl->initializeCacheData(bm_paths);

  m_dramCtrl->init();

  m_busArbiter->init();

  Simulator::Schedule(Seconds(0.0), &Step, this);
  Simulator::Stop(MilliSeconds(m_totalTimeInSeconds));
}

void MCoreSimProject::Step(MCoreSimProject *project)
{
  project->CycleProcess();
}

void MCoreSimProject::CycleProcess()
{
  bool SimulationDoneFlag = true;

  for (list<Ptr<CpuCoreGenerator>>::iterator it = m_cpuCoreGens.begin(); it != m_cpuCoreGens.end(); it++)
  {
    SimulationDoneFlag &= (*it)->GetCpuSimDoneFlag();
  }

  if (SimulationDoneFlag == true)
  {
    cout << "Current Simulation Done at Bus Clock Cycle # " << m_busCycle << endl;
    // cout << "L2 Nmiss =  " << m_SharedCacheCtrl->GetShareCacheMisses() << endl;
    // cout << "L2 NReq =  " << m_SharedCacheCtrl->GetShareCacheNReqs() << endl;
    // cout << "L2 Miss Rate =  " << (m_SharedCacheCtrl->GetShareCacheMisses() / (float)m_SharedCacheCtrl->GetShareCacheNReqs()) * 100 << endl;
    exit(0);
  }

  // Schedule the next run
  Simulator::Schedule(NanoSeconds(m_dt), &MCoreSimProject::Step, this);
  m_busCycle++;
}

void MCoreSimProject::EnableDebugFlag(bool Enable)
{

  // for (list<Ptr<CacheController>>::iterator it = m_cpuCacheCtrl.begin(); it != m_cpuCacheCtrl.end(); it++)
  // {
  //   (*it)->SetLogFileGenEnable(Enable);
  // }

  // m_SharedCacheCtrl->SetLogFileGenEnable(Enable);
  m_busArbiter->SetLogFileGenEnable(Enable);
}

void MCoreSimProject::GetCohrProtocolType()
{
  string cohType = m_projectXmlCfg.GetCohrProtType();
  if (cohType == "MSI")
  {
    m_cohrProt = CohProtType::SNOOP_MSI;
    m_llcCohrProt = CohProtType::SNOOP_LLC_MSI;
    m_fsm_protocol_path += "MSI_splitBus_snooping.csv";
    m_fsm_llc_protocol_path += "MSI_LLC.csv";
  }
  else if (cohType == "MESI")
  {
    m_cohrProt = CohProtType::SNOOP_MESI;
    m_llcCohrProt = CohProtType::SNOOP_LLC_MESI;
    m_fsm_protocol_path += "MESI_splitBus_snooping.csv";
    m_fsm_llc_protocol_path += "MESI_LLC.csv";
  }
  else if (cohType == "MOESI")
  {
    m_cohrProt = CohProtType::SNOOP_MOESI;
    m_llcCohrProt = CohProtType::SNOOP_LLC_MOESI;
    m_fsm_protocol_path += "MOESI_splitBus_snooping.csv";
    m_fsm_llc_protocol_path += "MOESI_LLC.csv";
  }
  else if (cohType == "PMSI")
  {
    m_cohrProt = CohProtType::SNOOP_PMSI;
    m_llcCohrProt = CohProtType::SNOOP_LLC_PMSI;
    m_fsm_protocol_path += "PMSI.csv";
    m_fsm_llc_protocol_path += "PMSI_LLC.csv";
  }
  else if (cohType == "PMESI")
  {
    m_cohrProt = CohProtType::SNOOP_PMESI;
    m_llcCohrProt = CohProtType::SNOOP_LLC_PMESI;
    m_fsm_protocol_path += "PMESI.csv";
    m_fsm_llc_protocol_path += "PMESI_LLC.csv";
  }
  else if (cohType == "PMSI_Asterisk")
  {
    m_cohrProt = CohProtType::SNOOP_PMSI_ASTERISK;
    m_llcCohrProt = CohProtType::SNOOP_LLC_PMSI_ASTERISK;
    m_fsm_protocol_path += "PMSI_asterisk.csv";
    m_fsm_llc_protocol_path += "PMSI_asterisk_LLC.csv";
  }
  else if (cohType == "PMESI_Asterisk")
  {
    m_cohrProt = CohProtType::SNOOP_PMESI_ASTERISK;
    m_llcCohrProt = CohProtType::SNOOP_LLC_PMESI_ASTERISK;
    m_fsm_protocol_path += "PMESI_asterisk.csv";
    m_fsm_llc_protocol_path += "PMESI_asterisk_LLC.csv";
  }
  else
  {
    std::cout << "Unsupported Coherence Protocol Cnfg Param = " << cohType << std::endl;
    exit(0);
  }
}

ReplcPolicy MCoreSimProject::ReplcPolicyDecode(std::string replcPolicy)
{
  ReplcPolicy policy;
  if (replcPolicy == "RANDOM")
  {
    policy = ReplcPolicy::RANDOM;
  }
  else if (replcPolicy == "LRU")
  {
    policy = ReplcPolicy::LRU;
  }
  else if (replcPolicy == "MRU")
  {
    policy = ReplcPolicy::MRU;
  }
  else if (replcPolicy == "LFU")
  {
    policy = ReplcPolicy::LFU;
  }
  else if (replcPolicy == "MFU")
  {
    policy = ReplcPolicy::MFU;
  }
  else if (replcPolicy == "FIFO")
  {
    policy = ReplcPolicy::FIFO;
  }
  else if (replcPolicy == "LIFO")
  {
    policy = ReplcPolicy::LIFO;
  }
  else
  {
    std::cout << "Unsupported Replc Policy Cnfg Param = " << replcPolicy << std::endl;
    exit(0);
  }
  return policy;
}
