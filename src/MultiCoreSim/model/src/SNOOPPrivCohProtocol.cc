/*
 * File  :      SNOOPPrivCohProtocol.cc
 * Author:      Salah Hessien
 * Email :      salahga@mcmaster.ca
 *
 * Created On May 30, 2020
 */

#include "../header/SNOOPPrivCohProtocol.h"

namespace ns3
{

  // override ns3 type
  TypeId SNOOPPrivCohProtocol::GetTypeId(void)
  {
    static TypeId tid = TypeId("ns3::SNOOPPrivCohProtocol")
                            .SetParent<Object>();
    return tid;
  }

  SNOOPPrivCohProtocol::SNOOPPrivCohProtocol()
  {
    // default
    m_pType = CohProtType::SNOOP_PMSI;
    m_processEvent = SNOOPPrivEventType::Core;
    m_coreId = 1;
    m_sharedMemId = 10;
    m_cache2Cache = false;
    m_logFileGenEnable = false;
    m_prllActionCnt = 0;
    m_reqWbRatio = 0;
    m_currEventTrans2Issue = NullTrans;
    m_replcPolicy = ReplcPolicy::RANDOM;
    uRnd1 = CreateObject<UniformRandomVariable>();
    uRnd1->SetAttribute("Min", DoubleValue(0));
    uRnd1->SetAttribute("Max", DoubleValue(100));
  }

  SNOOPPrivCohProtocol::SNOOPPrivCohProtocol(CohProtType ptype,
                                             bool logFileGenEnable, int coreId, int sharedMemId, bool cache2Cache, int reqWbRatio,
                                             Ptr<GenericCache> privCache, CpuFIFO* cpuFIFO, BusIfFIFO* busFIFO) : SNOOPPrivCohProtocol()
  {
    this->m_logFileGenEnable = logFileGenEnable;
    this->m_coreId = coreId;
    this->m_sharedMemId = sharedMemId;
    this->m_cache2Cache = cache2Cache;
    this->m_reqWbRatio = reqWbRatio;
    this->m_privCache = privCache;
    this->m_cpuFIFO = cpuFIFO;
    this->m_busIfFIFO = busFIFO;
    this->m_pType = ptype;

    switch (m_pType)
    {
    case CohProtType::SNOOP_PMSI:
      m_fsm_ptr = new PMSI;
      break;
    case CohProtType::SNOOP_MSI:
      m_fsm_ptr = new MSI;
      break;
    case CohProtType::SNOOP_MESI:
      m_fsm_ptr = new MESI;
      break;
    case CohProtType::SNOOP_MOESI:
      m_fsm_ptr = new MOESI;
      break;
    default:
      std::cout << "SNOOPPrivCohProtocol: unknown Snooping Protocol Type" << std::endl;
      exit(0);
    }
  }

  SNOOPPrivCohProtocol::~SNOOPPrivCohProtocol()
  {
    delete m_fsm_ptr;
  }

  CpuFIFO::ReqMsg SNOOPPrivCohProtocol::GetCpuReqMsg()
  {
    return m_msgList.cpuReqMsg;
  }

  void SNOOPPrivCohProtocol::SetCpuReqMsg(CpuFIFO::ReqMsg cpuReqMsg)
  {
    m_msgList.cpuReqMsg = cpuReqMsg;
  }

  BusIfFIFO::BusReqMsg SNOOPPrivCohProtocol::GetBusReqMsg()
  {
    return m_msgList.busReqMsg;
  }

  BusIfFIFO::BusRespMsg SNOOPPrivCohProtocol::GetBusRespMsg()
  {
    return m_msgList.busRespMsg;
  }

  SNOOPPrivCoreEvent SNOOPPrivCohProtocol::GetCpuReqEvent()
  {
    return m_eventList.cpuReqEvent;
  }

  void SNOOPPrivCohProtocol::SetCpuReqEvent(SNOOPPrivCoreEvent cpuReqEvent)
  {
    m_eventList.cpuReqEvent = cpuReqEvent;
  }

  SNOOPPrivReqBusEvent SNOOPPrivCohProtocol::GetBusReqEvent()
  {
    return m_eventList.busReqEvent;
  }

  SNOOPPrivRespBusEvent SNOOPPrivCohProtocol::GetBusRespEvent()
  {
    return m_eventList.busRespEvent;
  }

  SNOOPPrivEventType SNOOPPrivCohProtocol::GetCurrProcEvent()
  {
    return m_processEvent;
  }

  SNOOPPrivCtrlAction SNOOPPrivCohProtocol::GetCurrEventCtrlAction()
  {
    return m_ctrlAction;
  }

  void SNOOPPrivCohProtocol::SetCurrEventCtrlAction(SNOOPPrivCtrlAction currEvent)
  {
    m_ctrlAction = currEvent;
  }

  int SNOOPPrivCohProtocol::GetCurrEventCacheNextState()
  {
    return m_currEventNextState;
  }

  int SNOOPPrivCohProtocol::GetCurrEventCacheCurrState()
  {
    return m_currEventCurrState;
  }

  SNOOPPrivCohTrans SNOOPPrivCohProtocol::GetCurrEventCohrTrans()
  {
    return m_currEventTrans2Issue;
  }

  // EventID SNOOPPrivCohProtocol::getEvent(CpuFIFO::ReqMsg *cpu_msg)
  // {
  //   if (cpu_msg != NULL)
  //   {
  //     if (cpu_msg->type == CpuFIFO::REQTYPE::READ)
  //       return EventID::Load;
  //     else
  //       return EventID::Store;
  //   }
  //   return EventID::Null;
  // }

  // EventID SNOOPPrivCohProtocol::getEvent(BusIfFIFO::BusReqMsg *bus_req_msg)
  // {
  //   if (bus_req_msg != NULL)
  //   {
  //     int reqCoreId = bus_req_msg->reqCoreId;
  //     int wbCoreId = bus_req_msg->wbCoreId;
  //     switch (bus_req_msg->cohrMsgId)
  //     {
  //     case SNOOPPrivCohTrans::GetSTrans:
  //       return (reqCoreId == m_coreId) ? EventID::OwnGetS : EventID::OtherGetS;
  //     case SNOOPPrivCohTrans::GetMTrans:
  //       return (reqCoreId == m_coreId) ? EventID::OwnGetM : EventID::OtherGetM;
  //     case SNOOPPrivCohTrans::UpgTrans:
  //       return (reqCoreId == m_coreId) ? EventID::OwnUpg : EventID::OtherUpg;
  //     case SNOOPPrivCohTrans::PutMTrans:
  //       return (wbCoreId == m_coreId) ? EventID::OwnPutM : EventID::OtherPutM;
  //     case SNOOPPrivCohTrans::PutSTrans:
  //       return (wbCoreId == m_coreId) ? EventID::OwnPutS : EventID::OtherPutS;
  //     case SNOOPPrivCohTrans::ExclTrans:
  //       return (reqCoreId == m_coreId) ? EventID::OwnExclTrans : EventID::OtherExclTrans;
  //     case SNOOPPrivCohTrans::InvTrans:
  //       return EventID::OwnInvTrans;
  //     default: // Invalid Transaction
  //       std::cout << " SNOOPPrivCohProtocol: Invalid Transaction detected on the Bus" << std::endl;
  //       exit(0);
  //     }
  //   }
  //   return EventID::Null;
  // }

  // EventID SNOOPPrivCohProtocol::getEvent(BusIfFIFO::BusRespMsg *bus_resp_msg)
  // {
  //   if (bus_resp_msg != NULL)
  //   {
  //     if (m_cache2Cache == false)
  //     {
  //       return (bus_resp_msg->reqCoreId == m_coreId &&
  //               bus_resp_msg->respCoreId == m_sharedMemId)
  //                  ? EventID::OwnDataResp
  //                  : EventID::OtherDataResp;
  //     }
  //     else
  //     {
  //       return (bus_resp_msg->reqCoreId == m_coreId) ? EventID::OwnDataResp : EventID::OtherDataResp;
  //     }
  //   }
  //   return EventID::Null;
  // }

  void SNOOPPrivCohProtocol::ProcessCoreEvent()
  {
    uint32_t replcWayIdx;
    int emptyWayIdx;
    GenericCacheFrmt CacheLine;

    if (m_eventCacheInfoList.cpuReqCacheLineInfo.IsExist == false)
    {
      if (m_eventCacheInfoList.cpuReqCacheLineInfo.IsSetFull == true)
      {
        m_eventList.cpuReqEvent = SNOOPPrivCoreEvent::Replacement;
        replcWayIdx = m_privCache->GetReplacementLine(m_eventCacheInfoList.cpuReqCacheLineInfo.set_idx);
        CacheLine = m_privCache->ReadCacheLine(m_eventCacheInfoList.cpuReqCacheLineInfo.set_idx, replcWayIdx);
        m_eventCacheInfoList.cpuReqCacheLineInfo.way_idx = replcWayIdx;
        m_eventCacheInfoList.cpuReqCacheLineInfo.state = CacheLine.state;
        m_msgList.cpuReqMsg.addr = m_privCache->CpuPhyAddr(m_eventCacheInfoList.cpuReqCacheLineInfo.set_idx, replcWayIdx);
        m_msgList.cpuReqMsg.msgId = 0;
      }
      else
      {
        emptyWayIdx = m_privCache->GetEmptyCacheLine(m_eventCacheInfoList.cpuReqCacheLineInfo.set_idx);
        if (emptyWayIdx == -1)
        {
          std::cout << "SNOOPPrivCohProtocol: Fault non-empty Set condition !!!" << std::endl;
          exit(0);
        }
        CacheLine = m_privCache->ReadCacheLine(m_eventCacheInfoList.cpuReqCacheLineInfo.set_idx, emptyWayIdx);
        m_eventCacheInfoList.cpuReqCacheLineInfo.way_idx = emptyWayIdx;
        m_eventCacheInfoList.cpuReqCacheLineInfo.state = CacheLine.state;
      }
    }
    m_currEventCurrState = m_eventCacheInfoList.cpuReqCacheLineInfo.state;
    m_currEventNextState = m_eventCacheInfoList.cpuReqCacheLineInfo.state;
    CohProtocolFSMProcessing();
  }

  void SNOOPPrivCohProtocol::ProcessBusEvents()
  {
    if (m_processEvent == SNOOPPrivEventType::ReqBus)
    {
      m_currEventCurrState = m_eventCacheInfoList.busReqCacheLineInfo.state;
      m_currEventNextState = m_eventCacheInfoList.busReqCacheLineInfo.state;
    }
    else
    {
      m_currEventCurrState = m_eventCacheInfoList.busRespCacheLineInfo.state;
      m_currEventNextState = m_eventCacheInfoList.busRespCacheLineInfo.state;
    }

    if ((m_eventCacheInfoList.busReqCacheLineInfo.IsExist == false && m_processEvent == SNOOPPrivEventType::ReqBus) ||
        (m_eventCacheInfoList.busRespCacheLineInfo.IsExist == false && m_processEvent == SNOOPPrivEventType::RespBus))
    {
      m_currEventTrans2Issue = SNOOPPrivCohTrans::NullTrans;
      m_ctrlAction = SNOOPPrivCtrlAction::NoAck;
    }
    else
    {
      CohProtocolFSMProcessing();
    }
  }

  void SNOOPPrivCohProtocol::CohProtocolFSMProcessing()
  {
    IFCohProtocol *ptr;

    switch (m_pType)
    {
    case CohProtType::SNOOP_PMSI:
      ptr = new PMSI;
      break;
    case CohProtType::SNOOP_MSI:
      ptr = new MSI;
      break;
    case CohProtType::SNOOP_MESI:
      ptr = new MESI;
      break;
    case CohProtType::SNOOP_MOESI:
      ptr = new MOESI;
      break;
    default:
      std::cout << "SNOOPPrivCohProtocol: unknown Snooping Protocol Type" << std::endl;
      exit(0);
    }

    CohProtocolFSMBinding(ptr);
    delete ptr;
  }

  void SNOOPPrivCohProtocol::CohProtocolFSMBinding(IFCohProtocol *obj)
  {
    return obj->SNOOPPrivEventProcessing(m_processEvent,
                                         m_eventList,
                                         m_currEventNextState,
                                         m_currEventTrans2Issue,
                                         m_ctrlAction,
                                         m_cache2Cache);
  }

  int SNOOPPrivCohProtocol::InvalidCacheState()
  {
    switch (m_pType)
    {
    case CohProtType::SNOOP_PMSI:
      return static_cast<int>(SNOOP_PMSIPrivCacheState::I);
    case CohProtType::SNOOP_MSI:
      return static_cast<int>(SNOOP_MSIPrivCacheState::I);
    case CohProtType::SNOOP_MESI:
      return static_cast<int>(SNOOP_MESIPrivCacheState::I);
    case CohProtType::SNOOP_MOESI:
      return static_cast<int>(SNOOP_MOESIPrivCacheState::I);
    default:
      std::cout << "SNOOPPrivCohProtocol: unknown Snooping Protocol Type" << std::endl;
      exit(0);
    }
  }

  void SNOOPPrivCohProtocol::InitializeCacheStates()
  {
    int initState = InvalidCacheState();
    m_privCache->InitalizeCacheStates(initState);
  }

  void SNOOPPrivCohProtocol::GetEventsCacheInfo()
  {
    if (m_eventList.cpuReqEvent != SNOOPPrivCoreEvent::Null)
    {
      m_eventCacheInfoList.cpuReqCacheLineInfo = m_privCache->GetCacheLineInfo(m_msgList.cpuReqMsg.addr);
    }
    if (m_eventList.busReqEvent != SNOOPPrivReqBusEvent::Null)
    {
      m_eventCacheInfoList.busReqCacheLineInfo = m_privCache->GetCacheLineInfo(m_msgList.busReqMsg.addr);
    }
    if (m_eventList.busRespEvent != SNOOPPrivRespBusEvent::Null)
    {
      m_eventCacheInfoList.busRespCacheLineInfo = m_privCache->GetCacheLineInfo(m_msgList.busRespMsg.addr);
    }
  }

  SNOOPPrivEventPriority SNOOPPrivCohProtocol::PrivEventPriorityBinding(IFCohProtocol *obj)
  {
    return obj->PrivCacheEventPriority(m_eventList,
                                       m_eventCacheInfoList);
  }

  void SNOOPPrivCohProtocol::CohEventsSerialize()
  {
    SNOOPPrivEventPriority eventPriority;
    IFCohProtocol *ptr;
    switch (m_pType)
    {
    case CohProtType::SNOOP_PMSI:
      ptr = new PMSI;
      break;
    case CohProtType::SNOOP_MSI:
      ptr = new MSI;
      break;
    case CohProtType::SNOOP_MESI:
      ptr = new MESI;
      break;
    case CohProtType::SNOOP_MOESI:
      ptr = new MOESI;
      break;
    default:
      std::cout << "SNOOPPrivCohProtocol: unknown Snooping Protocol Type" << std::endl;
      exit(0);
    }

    eventPriority = PrivEventPriorityBinding(ptr);
    delete ptr;

    m_processEvent = SNOOPPrivEventType::Null;

    if (m_eventList.busReqEvent != SNOOPPrivReqBusEvent::Null &&
        m_eventList.busRespEvent != SNOOPPrivRespBusEvent::Null)
    {
      m_processEvent = (uRnd1->GetValue() <= 50) ? SNOOPPrivEventType::ReqBus : SNOOPPrivEventType::RespBus;
    }
    else if (m_eventList.busReqEvent != SNOOPPrivReqBusEvent::Null)
    {
      m_processEvent = SNOOPPrivEventType::ReqBus;
    }
    else if (m_eventList.busRespEvent != SNOOPPrivRespBusEvent::Null)
    {
      m_processEvent = SNOOPPrivEventType::RespBus;
    }
    if (eventPriority == SNOOPPrivEventPriority::ReqBus)
    {
      m_processEvent = SNOOPPrivEventType::ReqBus;
    }
    else if (eventPriority == SNOOPPrivEventPriority::RespBus)
    {
      m_processEvent = SNOOPPrivEventType::RespBus;
    }
    else if (eventPriority == SNOOPPrivEventPriority::WorkConserv)
    {
      if (m_eventList.cpuReqEvent != SNOOPPrivCoreEvent::Null)
      {
        if (m_eventList.busReqEvent != SNOOPPrivReqBusEvent::Null ||
            m_eventList.busRespEvent != SNOOPPrivRespBusEvent::Null)
        {
          m_prllActionCnt++;
          if (m_prllActionCnt > m_reqWbRatio)
          {
            m_prllActionCnt = 0;
          }
          else
          {
            m_processEvent = SNOOPPrivEventType::Core;
          }
        }
        else
        {
          m_processEvent = SNOOPPrivEventType::Core;
        }
      }
    }
  }

  GenericCache::CacheLineInfo SNOOPPrivCohProtocol::GetCurrEventCacheLineInfo()
  {
    switch (m_processEvent)
    {
    case SNOOPPrivEventType::Core:
      return m_eventCacheInfoList.cpuReqCacheLineInfo;
    case SNOOPPrivEventType::ReqBus:
      return m_eventCacheInfoList.busReqCacheLineInfo;
    case SNOOPPrivEventType::RespBus:
      return m_eventCacheInfoList.busRespCacheLineInfo;
    case SNOOPPrivEventType::Null:
      return m_eventCacheInfoList.cpuReqCacheLineInfo;
    default:
      std::cout << "SNOOPPrivCohProtocol: unknown Snooping Protocol Event" << std::endl;
      exit(0);
    }
  }

  void SNOOPPrivCohProtocol::UpdateCacheLine(CacheField field,
                                             GenericCacheFrmt cacheLineInfo,
                                             uint32_t set_idx,
                                             uint32_t way_idx)
  {
    if (field == State)
    {
      m_privCache->SetCacheLineState(set_idx, way_idx, cacheLineInfo);
    }
    else if (field == Line)
    {
      m_privCache->WriteCacheLine(set_idx, way_idx, cacheLineInfo);
    }
  }

  bool SNOOPPrivCohProtocol::IsCacheBlkValid(int state)
  {
    IFCohProtocol *obj;
    bool VldBlk = false;
    switch (m_pType)
    {
    case CohProtType::SNOOP_PMSI:
      obj = new (PMSI);
      break;
    case CohProtType::SNOOP_MSI:
      obj = new (MSI);
      break;
    case CohProtType::SNOOP_MESI:
      obj = new (MESI);
      break;
    case CohProtType::SNOOP_MOESI:
      obj = new (MOESI);
      break;
    default:
      std::cout << "SNOOPPrivCohProtocol: unknown Snooping Protocol Type" << std::endl;
      exit(0);
    }
    VldBlk = obj->IsValidBlk(state);
    delete obj;
    return VldBlk;
  }

  void SNOOPPrivCohProtocol::PrintEventInfo()
  {
    if (m_processEvent == SNOOPPrivEventType::Core)
    {
      std::cout << "\nSNOOPPrivCohProtocol: CoreId = " << m_coreId << " has Cpu ReqAction" << std::endl;
      std::cout << "\t\t Cpu Req Addr = " << m_msgList.cpuReqMsg.addr << " Type(0:Read) = " << m_msgList.cpuReqMsg.type << " CacheLine = " << m_eventCacheInfoList.cpuReqCacheLineInfo.cl_idx << std::endl;
      std::cout << "\t\t CacheLine Info: \n\t\t\t IsExist         =  " << m_eventCacheInfoList.cpuReqCacheLineInfo.IsExist << " \n\t\t\t IsValid         =  " << m_eventCacheInfoList.cpuReqCacheLineInfo.IsValid << " \n\t\t\t IsSetFull       =  " << m_eventCacheInfoList.cpuReqCacheLineInfo.IsSetFull << " \n\t\t\t Set_Idx         =  " << m_eventCacheInfoList.cpuReqCacheLineInfo.set_idx << " \n\t\t\t Way_Idx         =  " << m_eventCacheInfoList.cpuReqCacheLineInfo.way_idx << std::endl;
      std::cout << "\t\t\t CurrState       = " << PrintPrivStateName(m_currEventCurrState) << std::endl;
      std::cout << "\t\t\t NextState       = " << PrintPrivStateName(m_currEventNextState) << std::endl;
      std::cout << "\t\t\t Ctrl ReqAction  = " << PrintPrivActionName(m_ctrlAction) << std::endl;
      std::cout << "\t\t\t Ctrl ReqTrans   = " << PrintTransName(m_currEventTrans2Issue) << std::endl;
    }

    if (m_processEvent == SNOOPPrivEventType::ReqBus)
    {
      std::cout << "\nSNOOPPrivCohProtocol: CoreId = " << m_coreId << " has Msg on the RxReq Bus" << std::endl;
      std::cout << "\t\t BusEventName        = " << PrivReqBusEventName(m_eventList.busReqEvent) << std::endl;
      std::cout << "\t\t ReqCoreId           = " << m_msgList.busReqMsg.reqCoreId << ", RespCoreId = " << m_msgList.busReqMsg.wbCoreId << ", ReqMsgId = " << m_msgList.busReqMsg.msgId << ", Req Addr  = " << m_msgList.busReqMsg.addr << " CacheLine = " << m_eventCacheInfoList.busReqCacheLineInfo.cl_idx << std::endl;
      std::cout << "\t\t CacheLine Info: \n\t\t\t IsExist         =  " << m_eventCacheInfoList.busReqCacheLineInfo.IsExist << " \n\t\t\t IsValid         =  " << m_eventCacheInfoList.busReqCacheLineInfo.IsValid << " \n\t\t\t Set_Idx         =  " << m_eventCacheInfoList.busReqCacheLineInfo.set_idx << " \n\t\t\t Way_Idx         =  " << m_eventCacheInfoList.busReqCacheLineInfo.way_idx << std::endl;
      std::cout << "\t\t\t CurrState       = " << PrintPrivStateName(m_currEventCurrState) << std::endl;
      std::cout << "\t\t\t NextState       = " << PrintPrivStateName(m_currEventNextState) << std::endl;
      std::cout << "\t\t\t Ctrl ReqAction  = " << PrintPrivActionName(m_ctrlAction) << std::endl;
      std::cout << "\t\t\t Ctrl ReqTrans   = " << PrintTransName(m_currEventTrans2Issue) << std::endl;
    }

    if (m_processEvent == SNOOPPrivEventType::RespBus)
    {
      std::cout << "\nSNOOPPrivCohProtocol: CoreId = " << m_coreId << " has Msg on the RxResp Bus" << std::endl;
      std::cout << "\t\t BusEventName        = " << PrivRespBusEventName(m_eventList.busRespEvent) << std::endl;
      std::cout << "\t\t ReqCoreId           = " << m_msgList.busRespMsg.reqCoreId << ", RespCoreId = " << m_msgList.busRespMsg.respCoreId << ", ReqMsgId = " << m_msgList.busRespMsg.msgId << ", Req Addr  = " << m_msgList.busRespMsg.addr << " CacheLine = " << m_eventCacheInfoList.busRespCacheLineInfo.cl_idx << std::endl;
      std::cout << "\t\t CacheLine Info: \n\t\t\t IsExist         =  " << m_eventCacheInfoList.busRespCacheLineInfo.IsExist << " \n\t\t\t IsValid         =  " << m_eventCacheInfoList.busRespCacheLineInfo.IsValid << " \n\t\t\t Set_Idx         =  " << m_eventCacheInfoList.busRespCacheLineInfo.set_idx << " \n\t\t\t Way_Idx         =  " << m_eventCacheInfoList.busRespCacheLineInfo.way_idx << std::endl;
      std::cout << "\t\t\t CurrState       = " << PrintPrivStateName(m_currEventCurrState) << std::endl;
      std::cout << "\t\t\t NextState       = " << PrintPrivStateName(m_currEventNextState) << std::endl;
      std::cout << "\t\t\t Ctrl ReqAction  = " << PrintPrivActionName(m_ctrlAction) << std::endl;
      std::cout << "\t\t\t Ctrl ReqTrans   = " << PrintTransName(m_currEventTrans2Issue) << std::endl;
    }
  }

  std::string SNOOPPrivCohProtocol::PrintPrivStateName(int state)
  {
    IFCohProtocol *obj;
    std::string sName;
    switch (m_pType)
    {
    case CohProtType::SNOOP_PMSI:
      obj = new (PMSI);
      break;
    case CohProtType::SNOOP_MSI:
      obj = new (MSI);
      break;
    case CohProtType::SNOOP_MESI:
      obj = new (MESI);
      break;
    case CohProtType::SNOOP_MOESI:
      obj = new (MOESI);
      break;
    default:
      std::cout << "SNOOPPrivCohProtocol: unknown Snooping Protocol Type" << std::endl;
      exit(0);
    }
    sName = obj->PrivStateName(state);
    delete obj;
    return sName;
  }

  std::string SNOOPPrivCohProtocol::PrintPrivEventType(SNOOPPrivEventType event)
  {
    switch (event)
    {
    case SNOOPPrivEventType::Core:
      return " SNOOPPrivCohProtocol: Core Event is currently processed ";
    case SNOOPPrivEventType::ReqBus:
      return " SNOOPPrivCohProtocol: ReqBus Event is currently processed ";
    case SNOOPPrivEventType::RespBus:
      return " SNOOPPrivCohProtocol: RespBus Event is currently processed ";
    case SNOOPPrivEventType::Null:
      return " SNOOPPrivCohProtocol: No Events currently processed ";
    default:
      return " SNOOPPrivCohProtocol: Unknown Event!!!!";
    }
  }

  std::string SNOOPPrivCohProtocol::PrintPrivActionName(SNOOPPrivCtrlAction action)
  {
    switch (action)
    {
    case SNOOPPrivCtrlAction::Stall:
      return " Stall";
    case SNOOPPrivCtrlAction::Hit:
      return " Hit";
    case SNOOPPrivCtrlAction::issueTrans:
      return " issueTrans";
    case SNOOPPrivCtrlAction::ReissueTrans:
      return " ReissueTrans";
    case SNOOPPrivCtrlAction::issueTransSaveWbId:
      return " issueTransSaveWbId";
    case SNOOPPrivCtrlAction::WritBack:
      return " WritBack";
    case SNOOPPrivCtrlAction::CopyThenHit:
      return " CopyThenHit";
    case SNOOPPrivCtrlAction::CopyThenHitWB:
      return " CopyThenHitWB";
    case SNOOPPrivCtrlAction::CopyThenHitSendCoreOnly:
      return " CopyThenHitSendCoreOnly";
    case SNOOPPrivCtrlAction::CopyThenHitSendMemOnly:
      return " CopyThenHitSendMemOnly";
    case SNOOPPrivCtrlAction::CopyThenHitSendCoreMem:
      return " CopyThenHitSendCoreMem";
    case SNOOPPrivCtrlAction::SaveWbCoreId:
      return " SaveWbCoreId";
    case SNOOPPrivCtrlAction::HitSendMemOnly:
      return " HitSendMemOnly";
    case SNOOPPrivCtrlAction::SendMemOnly:
      return " SendMemOnly";
    case SNOOPPrivCtrlAction::SendCoreOnly:
      return " SendCoreOnly";
    case SNOOPPrivCtrlAction::SendCoreMem:
      return " SendCoreMem";
    case SNOOPPrivCtrlAction::Fault:
      return " Fault";
    case SNOOPPrivCtrlAction::NoAck:
      return " NoAck";
    case SNOOPPrivCtrlAction::NullAck:
      return " NullAck";
    case SNOOPPrivCtrlAction::ProcessedAck:
      return " ProcessedAck";
    default:
      return " Unknow Action";
    }
  }

  std::string SNOOPPrivCohProtocol::PrintTransName(SNOOPPrivCohTrans trans)
  {
    switch (trans)
    {
    case GetSTrans:
      return " GetSTrans ";
    case GetMTrans:
      return " GetMTrans ";
    case UpgTrans:
      return " UpgTrans ";
    case PutMTrans:
      return " PutMTrans ";
    case PutSTrans:
      return " PutSTrans ";
    case NullTrans:
      return " NullTrans ";
    default: // NullTrans:
      return " Unknow Trans";
    }
  }

  std::string SNOOPPrivCohProtocol::PrivReqBusEventName(SNOOPPrivReqBusEvent event)
  {
    switch (event)
    {
    case SNOOPPrivReqBusEvent::OwnGetS:
      return " OwnGetSEvent ";
    case SNOOPPrivReqBusEvent::OwnGetM:
      return " OwnGetMEvent ";
    case SNOOPPrivReqBusEvent::OwnPutM:
      return " OwnPutMEvent ";
    case SNOOPPrivReqBusEvent::OwnPutS:
      return " OwnPutSEvent ";
    case SNOOPPrivReqBusEvent::OwnUpg:
      return " OwnUpgEvent ";
    case SNOOPPrivReqBusEvent::OtherGetS:
      return " OtherGetSEvent ";
    case SNOOPPrivReqBusEvent::OtherGetM:
      return " OtherGetMEvent ";
    case SNOOPPrivReqBusEvent::OtherPutS:
      return " OtherPutSEvent ";
    case SNOOPPrivReqBusEvent::OtherPutM:
      return " OtherPutMEvent ";
    case SNOOPPrivReqBusEvent::OtherUpg:
      return " OtherUpgEvent ";
    case SNOOPPrivReqBusEvent::OwnExclTrans:
      return " OwnExclTrans ";
    case SNOOPPrivReqBusEvent::OtherExclTrans:
      return " OtherExclTrans ";
    case SNOOPPrivReqBusEvent::OwnInvTrans:
      return " OwnInvTrans ";
    case SNOOPPrivReqBusEvent::Null:
      return " NullEvent ";
    default:
      return " Unknown !!!!";
    }
  }

  std::string SNOOPPrivCohProtocol::PrivRespBusEventName(SNOOPPrivRespBusEvent event)
  {
    switch (event)
    {
    case SNOOPPrivRespBusEvent::OwnDataResp:
      return " OwnDataRespEvent ";
    case SNOOPPrivRespBusEvent::OwnDataRespExclusive:
      return " OwnDataRespExclusive ";
    case SNOOPPrivRespBusEvent::OtherDataResp:
      return " OtherDataRespEvent ";
    case SNOOPPrivRespBusEvent::Null:
      return " NullRespEvent ";
    default:
      return " Unknown !!!";
    }
  }

  SNOOPPrivCohTrans SNOOPPrivCohProtocol::GetCohTrans() { return m_currEventTrans2Issue; };

  SNOOPPrivCtrlAction SNOOPPrivCohProtocol::processRequest(CpuFIFO::ReqMsg *cpu_msg,
                                                           BusIfFIFO::BusReqMsg *bus_req_msg,
                                                           BusIfFIFO::BusRespMsg *bus_resp_msg)
  {
    // //I need to prioritize the requests like in CohEventsSerialize
    // SNOOPPrivEventType eventSource = SNOOPPrivEventType::Core; //it will be changed
    // EventID event_id;
    // if (eventSource == SNOOPPrivEventType::Core && (event_id = this->getEvent(cpu_msg)) != EventID::Null)
    // {
    //   GenericCache::CacheLineInfo line_info = m_privCache->GetCacheLineInfo(cpu_msg->addr);

    //   if (line_info.IsExist == false && line_info.IsSetFull == true)
    //   { /*Replacement should be handled here*/
    //   }
    //   else if (line_info.IsExist == false)
    //   {
    //     int emptyWayIdx = m_privCache->GetEmptyCacheLine(line_info.set_idx);
    //     if (emptyWayIdx == -1)
    //     {
    //       std::cout << "SNOOPPrivCohProtocol: Fault non-empty Set condition !!!" << std::endl;
    //       exit(0);
    //     }
    //     m_eventCacheInfoList.cpuReqCacheLineInfo.way_idx = emptyWayIdx;
    //     m_eventCacheInfoList.cpuReqCacheLineInfo.state = m_privCache->ReadCacheLine(line_info.set_idx, emptyWayIdx).state; //should be replaced by I
    //   }
    // }

    // int next_state = m_eventCacheInfoList.cpuReqCacheLineInfo.state;
    // SNOOPPrivCohTrans coh_trans = SNOOPPrivCohTrans::NullTrans;
    // SNOOPPrivCtrlAction action = SNOOPPrivCtrlAction::NoAck;
    // m_fsm_ptr->SNOOPPrivEventProcessing(eventSource, m_eventList, //will be changed to EventID
    //                                      next_state, coh_trans, action, m_cache2Cache);

    return SNOOPPrivCtrlAction::Fault;
  }
}
