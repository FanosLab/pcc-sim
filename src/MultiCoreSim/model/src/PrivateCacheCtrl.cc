/*
 * File  :      MCoreSimProjectXml.h
 * Author:      Salah Hessien
 * Email :      salahga@mcmaster.ca
 *
 * Created On February 17, 2020
 */

#include "../header/PrivateCacheCtrl.h"

namespace ns3
{

  // override ns3 type
  TypeId PrivateCacheCtrl::GetTypeId(void)
  {
    static TypeId tid = TypeId("ns3::PrivateCacheCtrl")
                            .SetParent<Object>();
    return tid;
  }

  // private controller constructor
  PrivateCacheCtrl::PrivateCacheCtrl(CacheXml &cacheXml,
                                     BusIfFIFO* assoicateBusIfFIFO, CpuFIFO* associatedCpuFIFO,
                                     bool cach2Cache, bool logFileGenEnable, int sharedMemId,
                                     CohProtType pType)
  {
    m_cacheCycle = 1;

    uint32_t cacheLines = cacheXml.GetCacheSize() / cacheXml.GetBlockSize();

    m_cacheType = cacheXml.GetMappingType();
    m_cacheSize = cacheXml.GetCacheSize();
    m_cacheBlkSize = cacheXml.GetBlockSize();
    m_nways = cacheXml.GetNWays();
    m_nsets = cacheLines / cacheXml.GetNWays();
    m_coreId = cacheXml.GetCacheId();
    m_reqWbRatio = cacheXml.GetReqWbRatio();
    m_maxPendingReq = cacheXml.GetNPendReq();
    m_pendingCpuReq = cacheXml.GetNPendReq() * 2;
    m_dt = cacheXml.GetCtrlClkNanoSec();
    m_clkSkew = m_dt * cacheXml.GetCtrlClkSkew() / 100.00;

    m_busIfFIFO = assoicateBusIfFIFO;
    m_cpuFIFO = associatedCpuFIFO;
    m_cpuPendingFIFO = CreateObject<GenericFIFO<PendingMsg>>();                   // TODO: cpu pending fifo isn't bounded?
    m_PendingWbFIFO.SetFifoDepth(assoicateBusIfFIFO->m_txMsgFIFO.GetFifoDepth()); // TODO: Pending WbFIFO's bound is the same as BusIfFifo?

    m_cache = CreateObject<GenericCache>(cacheLines);
    m_cache->SetCacheType(m_cacheType);

    m_cach2Cache = cach2Cache;
    m_logFileGenEnable = logFileGenEnable;

    m_sharedMemId = sharedMemId;

    m_pType = pType;

    m_cohProtocol = new SNOOPPrivCohProtocol(m_pType, m_logFileGenEnable, m_coreId, m_sharedMemId,
                                             m_cach2Cache, m_reqWbRatio, m_cache, m_cpuFIFO, m_busIfFIFO);

    // /*
    //  * print configurations
    //  */
    // if (m_logFileGenEnable)
    // {
    //   cout << "\nCpuCore       " << PrivateCacheXml.GetCacheId() << ", Clk (NanoSec) = " << PrivateCacheXml.GetCpuClkNanoSec() << ", Clk-Skew (ns) = " << cpuClkSkew << endl;
    //   cout << "PrivCacheCtrl " << PrivateCacheXml.GetCacheId() << ", Clk (NanoSec) = " << PrivateCacheXml.GetCtrlClkNanoSec() << ", Clk-Skew (ns) = " << ctrlClkSkew << ", Req2Writeback Ratio = " << PrivateCacheXml.GetReqWbRatio() << " %" << endl;
    //   cout << "\tPrivCache: BlkSize (Bytes) = " << PrivateCacheXml.GetBlockSize() << ", CacheSize (Bytes) = " << PrivateCacheXml.GetCacheSize() << ", MappingType (0:direct, 1: associative) = " << PrivateCacheXml.GetMappingType() << ", Nways = " << PrivateCacheXml.GetNWays() << ", Nsets = " << nsets << endl;
    // }
  }

  PrivateCacheCtrl::~PrivateCacheCtrl()
  {
    delete m_cohProtocol;
  }

  // Set Maximum number of Pending CPU Request (OOO)
  void PrivateCacheCtrl::SetMaxPendingReq(int maxPendingReq)
  {
    m_maxPendingReq = maxPendingReq;
  }

  // Set Pending CPU FIFI depth
  void PrivateCacheCtrl::SetPendingCpuFIFODepth(int size)
  {
    m_cpuPendingFIFO->SetFifoDepth(size);
  }

  void PrivateCacheCtrl::SetCacheSize(uint32_t cacheSize)
  {
    m_cacheSize = cacheSize;
    m_cache->SetCacheSize(cacheSize);
  }

  uint32_t PrivateCacheCtrl::GetCacheSize()
  {
    return m_cacheSize;
  }

  void PrivateCacheCtrl::SetCacheBlkSize(uint32_t cacheBlkSize)
  {
    m_cacheBlkSize = cacheBlkSize;
    m_cache->SetCacheBlkSize(cacheBlkSize);
  }

  uint32_t PrivateCacheCtrl::GetCacheBlkSize()
  {
    return m_cacheBlkSize;
  }

  void PrivateCacheCtrl::SetCacheNways(uint32_t nways)
  {
    m_nways = nways;
    m_cache->SetCacheNways(nways);
  }

  uint32_t PrivateCacheCtrl::GetCacheNways()
  {
    return m_nways;
  }

  void PrivateCacheCtrl::SetCacheNsets(uint32_t nsets)
  {
    m_nsets = nsets;
    m_cache->SetCacheNsets(nsets);
  }

  uint32_t PrivateCacheCtrl::GetCacheNsets()
  {
    return m_nsets;
  }

  void PrivateCacheCtrl::SetCacheType(uint16_t cacheType)
  {
    m_cacheType = cacheType;
    m_cache->SetCacheType(cacheType);
  }

  uint16_t PrivateCacheCtrl::GetCacheType()
  {
    return m_cacheType;
  }

  void PrivateCacheCtrl::SetCoreId(int coreId)
  {
    m_coreId = coreId;
  }

  void PrivateCacheCtrl::SetSharedMemId(int sharedMemId)
  {
    m_sharedMemId = sharedMemId;
  }

  int PrivateCacheCtrl::GetCoreId()
  {
    return m_coreId;
  }

  void PrivateCacheCtrl::SetDt(double dt)
  {
    m_dt = dt;
  }

  int PrivateCacheCtrl::GetDt()
  {
    return m_dt;
  }

  void PrivateCacheCtrl::SetClkSkew(double clkSkew)
  {
    m_clkSkew = clkSkew;
  }

  void PrivateCacheCtrl::SetReqWbRatio(int reqWbRatio)
  {
    m_reqWbRatio = reqWbRatio;
  }

  void PrivateCacheCtrl::SetCache2Cache(bool cach2Cache)
  {
    m_cach2Cache = cach2Cache;
  }

  // Set Coherence Protocol Type
  void PrivateCacheCtrl::SetProtocolType(CohProtType ptype)
  {
    m_pType = ptype;
  }

  void PrivateCacheCtrl::SetLogFileGenEnable(bool logFileGenEnable)
  {
    m_logFileGenEnable = logFileGenEnable;
  }

  // insert new Transaction into BusTxMsg FIFO
  bool PrivateCacheCtrl::PushMsgInBusTxFIFO(uint64_t msgId,
                                            uint16_t reqCoreId,
                                            uint16_t wbCoreId,
                                            uint16_t transId,
                                            uint64_t addr,
                                            bool PendingWbBuf = false,
                                            bool NoGetMResp = false)
  {
    BusIfFIFO::BusReqMsg tempBusReqMsg;
    tempBusReqMsg.msgId = msgId;
    tempBusReqMsg.reqCoreId = reqCoreId;
    tempBusReqMsg.wbCoreId = wbCoreId;
    tempBusReqMsg.cohrMsgId = transId;
    tempBusReqMsg.addr = addr;
    tempBusReqMsg.timestamp = m_cacheCycle * m_dt;
    tempBusReqMsg.cycle = m_cacheCycle;
    tempBusReqMsg.NoGetMResp = NoGetMResp;

    if (!m_busIfFIFO->m_txMsgFIFO.IsFull() && PendingWbBuf == false)
    {
      // push message into BusTxMsg FIFO
      m_busIfFIFO->m_txMsgFIFO.InsertElement(tempBusReqMsg);
      return true;
    }
    else if (!m_PendingWbFIFO.IsFull() && PendingWbBuf == true)
    {
      // push message into BusTxMsg FIFO
      m_PendingWbFIFO.InsertElement(tempBusReqMsg);
      return true;
    }
    else
    {
      if (m_logFileGenEnable)
      {
        std::cout << "Info: Cannot insert the Msg into BusTxMsg FIFO, FIFO is Full" << std::endl;
      }
      return false;
    }
  }

  bool PrivateCacheCtrl::MOESI_Modify_NoGetMResp_TxFIFO(uint64_t addr)
  {
    int pendingQueueSize = m_busIfFIFO->m_txMsgFIFO.GetQueueSize();
    bool ModifyDone = false;
    GenericCacheMapFrmt addrMap, TxBufAddrMap;
    addrMap = m_cache->CpuAddrMap(addr);
    BusIfFIFO::BusReqMsg pendingTxMsg;
    for (int i = 0; i < pendingQueueSize; i++)
    {
      pendingTxMsg = m_busIfFIFO->m_txMsgFIFO.GetFrontElement();
      m_busIfFIFO->m_txMsgFIFO.PopElement();
      TxBufAddrMap = m_cache->CpuAddrMap(pendingTxMsg.addr);
      if (addrMap.idx_set == TxBufAddrMap.idx_set && addrMap.tag == TxBufAddrMap.tag && pendingTxMsg.cohrMsgId == SNOOPPrivCohTrans::GetMTrans)
      {
        pendingTxMsg.NoGetMResp = false;
        ModifyDone = true;
      }
      m_busIfFIFO->m_txMsgFIFO.InsertElement(pendingTxMsg);
    }
    return ModifyDone;
  }

  // execute write back command
  bool PrivateCacheCtrl::DoWriteBack(uint64_t addr, uint16_t wbCoreId, uint64_t msgId, bool dualTrans = false)
  {
    if (!m_busIfFIFO->m_txRespFIFO.IsFull())
    {
      GenericCacheMapFrmt addrMap = m_cache->CpuAddrMap(addr);
      GenericCacheFrmt wbLine = m_cache->ReadCacheLine(addrMap.idx_set);
      BusIfFIFO::BusRespMsg wbMsg;
      wbMsg.reqCoreId = wbCoreId;
      wbMsg.respCoreId = m_coreId;
      wbMsg.addr = addr;
      wbMsg.timestamp = m_cacheCycle * m_dt;
      wbMsg.cycle = m_cacheCycle;
      wbMsg.msgId = msgId;
      wbMsg.dualTrans = dualTrans;
      if (m_logFileGenEnable)
      {
        std::cout << "DoWriteBack:: coreId = " << m_coreId << " requested Core = " << wbCoreId << std::endl;
      }
      for (int i = 0; i < 8; i++)
        wbMsg.data[i] = wbLine.data[i];
      // push message into BusTxMsg FIFO
      m_busIfFIFO->m_txRespFIFO.InsertElement(wbMsg);
      return true;
    }
    else
    {
      if (m_logFileGenEnable)
      {
        std::cout << "Info: Cannot insert the Msg into BusTxResp FIFO, FIFO is Full" << std::endl;
        std::cout << "TxResp Buffer Size = " << m_busIfFIFO->m_txRespFIFO.GetQueueSize() << std::endl;
      }
      return false;
    }
  }

  // process pending buffer
  bool PrivateCacheCtrl::SendPendingWB(GenericCacheMapFrmt recvTrans, TransType type = MemOnly)
  {
    bool ReqSentFlag = false;
    // check if there is a pending write-back to this line in the pending buffer
    if (!m_PendingWbFIFO.IsEmpty())
    {
      BusIfFIFO::BusReqMsg pendingWbMsg;
      GenericCacheMapFrmt pendingWbAddrMap;
      int pendingQueueSize = m_PendingWbFIFO.GetQueueSize();
      for (int i = 0; i < pendingQueueSize; i++)
      {
        pendingWbMsg = m_PendingWbFIFO.GetFrontElement();
        pendingWbAddrMap = m_cache->CpuAddrMap(pendingWbMsg.addr);
        // Remove message from the busReq buffer
        m_PendingWbFIFO.PopElement();
        if (recvTrans.idx_set == pendingWbAddrMap.idx_set &&
            recvTrans.tag == pendingWbAddrMap.tag)
        {
          // b.3.2)send data to requestors
          uint16_t reqCoreId = (type == CoreOnly || type == CorePlsMem) ? pendingWbMsg.reqCoreId : m_sharedMemId;
          bool dualTrans = (type == CorePlsMem);
          if (!DoWriteBack(pendingWbMsg.addr, reqCoreId, pendingWbMsg.msgId, dualTrans))
          {
            std::cout << "PrivCache " << m_coreId << " TxResp FIFO is Full!" << std::endl;
            exit(0);
          }
          ReqSentFlag = true;
        }
        else
        {
          // b.3.1) Dequeue the data again into pending buffer
          m_PendingWbFIFO.InsertElement(pendingWbMsg);
        }
      }
    }
    return ReqSentFlag;
  }

  bool PrivateCacheCtrl::PendingCoreBufRemoveMsg(uint64_t msgId, PendingMsg &removedMsg)
  {
    bool removalFlg = false;
    int pendingBufSize = m_cpuPendingFIFO->GetQueueSize();
    PendingMsg cpuPendingMsg;
    for (int i = 0; i < pendingBufSize; i++)
    {
      cpuPendingMsg = m_cpuPendingFIFO->GetFrontElement();
      m_cpuPendingFIFO->PopElement();
      if (m_logFileGenEnable)
      {
        std::cout << "PendingCpuBuffer: Entery = " << i << ", core Id = " << m_coreId << " cpuMsg.msgId = " << cpuPendingMsg.cpuMsg.msgId << " request.msgId = " << msgId << ", addr = " << cpuPendingMsg.cpuMsg.addr << " IsProcessed = " << cpuPendingMsg.IsProcessed << std::endl;
      }
      // Remove message that has same Id from the pending buffer
      if (cpuPendingMsg.cpuMsg.msgId == msgId)
      {
        removalFlg = true;
        removedMsg = cpuPendingMsg;
        if (m_logFileGenEnable)
        {
          std::cout << "PendingCpuBuffer: Core Id = " << m_coreId << " New message get removed from the Pending Buffer, Pending Cnt " << m_pendingCpuReq << std::endl;
        }
      }
      else
      {
        m_cpuPendingFIFO->InsertElement(cpuPendingMsg); // dequeue
      }
    }
    return removalFlg;
  }

  // This function does most of the functionality.
  void PrivateCacheCtrl::CacheCtrlMain()
  {
    CpuFIFO::ReqMsg *pcpu_msg = NULL;
    BusIfFIFO::BusReqMsg *pbus_req_msg = NULL; 
    BusIfFIFO::BusRespMsg *pbus_resp_msg = NULL;
    CpuFIFO::ReqMsg cpu_msg;
    BusIfFIFO::BusReqMsg bus_req_msg; 
    BusIfFIFO::BusRespMsg bus_resp_msg;
    if(!m_cpuFIFO->m_txFIFO.IsEmpty())
    {
      cpu_msg = m_cpuFIFO->m_txFIFO.GetFrontElement();
      pcpu_msg = &cpu_msg;
    }
    if (!m_busIfFIFO->m_rxMsgFIFO.IsEmpty())
    {
      bus_req_msg = m_busIfFIFO->m_rxMsgFIFO.GetFrontElement();
      pbus_req_msg = &bus_req_msg;
    }
    if (!m_busIfFIFO->m_rxRespFIFO.IsEmpty())
    {
      bus_resp_msg = m_busIfFIFO->m_rxRespFIFO.GetFrontElement();
      pbus_resp_msg = &bus_resp_msg;
    }

    m_cohProtocol->processRequest(pcpu_msg, pbus_req_msg, pbus_resp_msg);
  }

  void PrivateCacheCtrl::CycleProcess()
  {
    // Call cache controller
    CacheCtrlMain();
    // Schedule the next run
    Simulator::Schedule(NanoSeconds(m_dt), &PrivateCacheCtrl::Step, Ptr<PrivateCacheCtrl>(this));
    m_cacheCycle++;
  }

  // The init function starts the controller at the beginning
  void PrivateCacheCtrl::init()
  {
    // Initialized Cache Coherence Protocol
    m_cohProtocol->InitializeCacheStates();
    Simulator::Schedule(NanoSeconds(m_clkSkew), &PrivateCacheCtrl::Step, Ptr<PrivateCacheCtrl>(this));
  }

  /**
     * Runs one mobility Step for the given vehicle generator.
     * This function is called each interval dt
     */
  void PrivateCacheCtrl::Step(Ptr<PrivateCacheCtrl> privateCacheCtrl)
  {
    privateCacheCtrl->CycleProcess();
  }
}
